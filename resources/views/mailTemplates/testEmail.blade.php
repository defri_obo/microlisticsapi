<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Email</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}"> -->
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
    <style>
        body{
            font-size: 18px;
        }
        .background-blue{
            background-image: url('https://img.techpowerup.org/200624/homebackground.png');
            background-size: cover;
            background-repeat: no-repeat;
        }

        .mls-logo{
            background-image: url({{asset('asset/logoWhite.png')}})
        }
        p {
            margin-top: 0;
            margin-bottom: 0rem !important;
        }
        .f-10{
            font-size:10px;
        }
        .bold{
            font-weight: 700!important;
        }
        .medium-bold{
            font-weight: 600!important;
        }
        .small-bold{
            font-weight: 400!important;
        }
        .grey{
            color: #637175;
        }
        .logo-white{
          width: 90px;
          height: 50px;
        }
        .body-card{
            background-color: white;
            border-radius:10px;
            padding:1rem;
        }
        .row{
            margin-right: 0px !important;
            margin-left: 0px !important;
        }

        .sub-text{
            color: #959DA0;
            opacity: 1;
        }
        .content{
            padding: 2rem 3rem 2rem 3rem;
        }
        .border{
            border: 2px solid #3D2CE3 !important;
            opacity: 1;
            border-radius:10px
        }

        .w-40{
            width:47%;
        }

        .d-flex{
            display:flex;
        }

        .justify-content-center{
            justify-content: center;
        }

        .mt-5{
            margin-top:2rem;
        }

        .mt1{

            margin-top:.25rem;
        }
        .ml-1{
            margin-left:.25rem;
        }

        .mr-1{
            margin-right:.25rem;
        }

        .mb-5{
            margin-bottom: 2rem;
        }
/*
        .h-100{
            height:18.5vw;
        } */

        .px-5{
            padding-left: 3rem;
            padding-right:2.7rem;
        }

        .mb-3{
            margin-bottom:1rem;
        }

        .mb-2{
            margin-bottom:.50rem;
        }


    </style>
  </head>
  <body class="d-flex justify-content-center">
  <div class="d-flex flex-column w-40 h-100 border background-blue">
        <div class="mt-1 ml-1 mr-1 mb-1">
            <div class="mt-5 px-5">
                <div class="row bold">
                      Reset Password
                </div>
                <div class="row medium-bold mb-3 sub-text">
                    To: {!! $name !!}
                </div>
            </div>
            <div class="row mt-3 small-bold body-card mb-3 px-5">
                <label class='w-100 p-3'>Hi  {!! $name !!}, </br>
                <p class="sub-text">
                <span class="grey">
                    Your password has been changed, please use new password to login
                </span>
                </p>
                </label>

                <table
                    <tr>
                        <td>User Id</td>
                        <td>{!! $userId !!}</td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>{!! $new_password !!}</td>
                    </tr>
                </table>
            </div>
            <div class="d-flex px-5 mb-2">
                <img class="logo-white" src='https://img.techpowerup.org/200624/logo-4.png'>
                </img>
            </div>
            <div class="mb-5 px-5">
                <p class="f-10">
                    Copyright @ {{ date('Y') }} Microlistic - part of the WiseTech Global Group
                </p>
                <p class="f-10 bold">
                    If you any assistance in this matter, please contact <a style="text-decoration: none;">&nbsp;</a><a href="https://www.microlistics.com.au/service-and-support/"> Microlistics Help</a>
                </p>
                <p class="f-10">
                    <a href='http://microlistics.tech'>Privacy policy</a> <a style="text-decoration: none;">&nbsp;</a>|<a style="text-decoration: none;">&nbsp;</a><a href='http://microlistics.tech'>Terms and Conditions</a>
                </p>
            </div>

        </div>

    </div>


  </body>
</html>
