<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// =======
// Route::get('loadProduct', 'StockHoldingController@loadProduct');
// Route::get('loadSHDetail', 'StockHoldingController@loadStockHolding');
// Route::get('loadForeshadow', 'StockHoldingController@loadForeshadow');
// Route::get('searchStockHolding', 'StockHoldingController@searchStockHolding');

// //Stock Age Profile Api
// Route::get('loadStockAgeProfile', 'StockAgeProfileController@loadStockAgeProfile');
// Route::get('searchStockAgeProfile', 'StockAgeProfileController@searchStockAgeProfile');
// Route::get('filterStockAgeProfile', 'StockAgeProfileController@filterStockAgeProfile');
// Route::get('searchAndFilterStockAgeProfile', 'StockAgeProfileController@searchAndFilterStockAgeProfile');
// >>>>>>> 46731e7315e66a95aadeb693f7c0f89b943f890c

Route::get('/isstock', 'IsstockController@Isstock');//get all stockholding

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
