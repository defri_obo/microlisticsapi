<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/stockholding', 'StockholdingController@index');//get all stockholding
Route::get('/foreshadowedstockbalance/{id}', 'ForeshadowedStockBalanceController@show');//get foreshadowedstockbalance by id
Route::get('/foreshadowedstockbalance', 'ForeshadowedStockBalanceController@index');//get all foreshadowedstockbalance
Route::any('/stockbal', 'ForeshadowedStockBalanceController@stockbal');//get foreshadowedstockbalance
Route::get('/stockdetail', 'StockDetailController@index');//index stock detail
Route::get('/stockdetail/{id}', 'StockDetailController@show');//get foreshadowedstockbalance by id
Route::get('/stockdetail/header/{id}', 'StockDetailController@showHeader');//get stock details header by id
Route::get('/stockdetail/forshadowed/{id}', 'StockDetailController@getForeShadowed');//get stock details header by id
Route::get('/stockageprofile', 'StockAgeProfileController@searchFilterStockAgeProfile');//Stock Age Profile Api
Route::post('/userlogin/refreshtoken', 'UserLoginController@refreshToken');//Refresh token
// Route::post('/userlogin', 'UserLoginController@userLogin');//Login Auth API
Route::get('/stockmovement', 'StockMovementController@loadStockMovement');//Stock Movement API
Route::get('/stockdaterange', 'StockMovementController@stockDateRange');//Stock Movement API
Route::get('/customizetable', 'CustomizeTableController@testes');//Customize Table Controller API

//Purchase Order API
Route::get('/purchaseOrder', 'PurchaseOrderController@index');//get all purchase order
Route::get('/purchaseOrder/{site}/{client}/{product}', 'PurchaseOrderController@purchaseOrderDetail');//get all purchase order
Route::post('/purchaseOrder/store', 'PurchaseOrderController@store');//get all purchase order
Route::get('/getporecources', 'PurchaseOrderController@getPoResourceCreation'); //get field resource in order creation

//Sales Order API
Route::get('/salesorder', 'SalesOrderController@index');
Route::get('/salesorder/{orderNo}', 'SalesOrderController@detail');
Route::get('/getsorecources', 'SalesOrderController@getSoResourceCreation'); //get field resource in order creation
Route::get('/getSoIdentity', 'SalesOrderController@getSoIdentity');
Route::post('/salesorder/store', 'SalesOrderController@store'); //create sales order

//User Management API
Route::get('usermanagement/module', 'UserManagementController@module'); //get all module
Route::get('usermanagement/clientsite', 'UserManagementController@getClientAndSite'); //get all module
Route::post('usermanagement/register', 'UserManagementController@createUser'); // Create user
Route::post('usermanagement/update/{id}', 'UserManagementController@updateUser'); // Update User
Route::post('usermanagement/login', 'UserManagementController@login'); // User Login
Route::post('usermanagement/logout', 'UserManagementController@logout'); // User Logout
Route::post('usermanagement/forgot_password', 'UserManagementController@forgot_password'); // Reset Email
Route::post('usermanagement/request_reset_password', 'UserManagementController@request_reset_password'); // Reset Password Request

// Route::post('forgot_password', 'ResetPasswordController@forgot_password')->name('password.reset');
// Route::post('change_password', 'ResetPasswordController@change_password');
// Route::post('test_email', 'ResetPasswordController@test_email');
// Route::post('sendResetLinkEmail', 'ResetPasswordController@sendResetLinkEmail');

//Master Dropdown API
Route::get('/dropdown/getclient', 'DropDownController@getClient');
Route::get('/dropdown/getsite', 'DropDownController@getSite');
Route::get('/dropdown/getsupplier', 'DropDownController@getSupplier');
Route::get('/dropdown/getordertype', 'DropDownController@getOrderType');
Route::get('/dropdown/getuom', 'DropDownController@getUom');
Route::get('/dropdown/getdisposition', 'DropDownController@getDisposition');
Route::get('/dropdown/getPoOrderType', 'DropDownController@getPoOrderType');
Route::get('/dropdown/getIsisTask', 'DropDownController@getIsisTask');

//Dropdown Product
Route::get('/dropdown/getProduct', 'ProductController@getProduct');

Route::get('/web_user', 'Iw_web_user_controller@index');//get all user
Route::get('/web_user_detail/{web_user}', 'Iw_web_user_controller@show');

//Order Check
Route::post('orderCheck', 'OrderCheckController@index'); // User Login

//Email Check
Route::post('emailCheck', 'UserManagementController@emailCheck');

//renaming column
Route::get('/getStockholdingColumn', 'RenameColumnController@getStockholdingColumn');
Route::post('/putStockholdingColumn', 'RenameColumnController@putStockholdingColumn');
Route::get('/getSalesOrderColumn', 'RenameColumnController@getSalesOrderColumn');
Route::post('/putSalesOrderColumn', 'RenameColumnController@putSalesOrderColumn'); 
Route::get('/getSalesOrderDetailColumn', 'RenameColumnController@getSalesOrderDetailColumn');
Route::post('/putSalesOrderDetailColumn', 'RenameColumnController@putSalesOrderDetailColumn');
Route::get('/getPurchaseOrderColumn', 'RenameColumnController@getPurchaseOrderColumn');
Route::post('/putPurchaseOrderColumn', 'RenameColumnController@putPurchaseOrderColumn');
Route::get('/getPurchaseOrderDetailColumn', 'RenameColumnController@getPurchaseOrderDetailColumn');
Route::post('/putPurchaseOrderDetailColumn', 'RenameColumnController@putPurchaseOrderDetailColumn');

