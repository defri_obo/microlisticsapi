<?php

namespace App\Models\Ml_user;
use Illuminate\Database\Eloquent\Model;

class Ml_User Extends Model
{
    //
    protected $table = 'ADM.ML_USERS';

    protected $fillable = [
        'ML_USERSID',
        'ML_USERSPASSWORD',
        'ML_ACCESSGROUPSID',
    ];
}