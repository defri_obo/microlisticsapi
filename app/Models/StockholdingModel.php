<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class StockholdingModel Extends Model
{
    //
    protected $table = 'IW_STOCK_HOLDING_V';
}