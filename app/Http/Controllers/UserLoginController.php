<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use App\User;
class UserLoginController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');  
    }
    
    public function userLogin(Request $request){

           
        $userId = $request->userid;
        $passwordLogin = $request->password;
        try{
            $userLogin = DB::table('iw_web_user')
                           ->where('userid', '=', $userId)
                           ->first();
            if($userLogin == null){
                return response()->json(['error message'=> 'username not exists'],404);
            }

            $userLogin = (array)$userLogin;                
            $password = $userLogin['password'];

            if(!Hash::check($passwordLogin, $password)){
                return response()->json(['error message'=> 'invalid username or password'],401);
            }

            $credentials = $request->only('userid', 'password');
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            
            $token = compact('token');
            $merges = (array)$token;
            $tokenValue = $merges['token']; 
            $companyCode = $userLogin['client'];
            $userLevel = $userLogin['web_group']; 
            $result = [
                        'companyCode'=>Crypt::encrypt($companyCode),                       
                        'userLevel'=>Crypt::encrypt($userLevel),
                        'token' => $tokenValue
                      ];   
                return response()->json($result,200);
        }
        catch(Exception $e){
            return response()->json(['errorMessage'=> 'something wrong'],500);
        }
    }

    public function refreshToken(Request $request)
    {
            $token = JWTAuth::getToken();

            if(!$token)
                {
                    return response()->json(['status' => 'token is Required'],401);
                }

            try
            {
                $refreshed = JWTAuth::refresh($token);             
                return response()->json(['token' => $refreshed],200);
            }
            catch(Exception $e){
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException)
                {
                    return response()->json(['status' => 'Token is Blacklisted'],401);
                }
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
                {
                    return response()->json(['status' => 'Token is Invalid'],400);
                }
                
            }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|unique:iw_web_user,userid|string|max:30',
            'password' => 'required|max:12',
            'name' => 'required|max:30',
            'email' => 'required|max:40',
            'lastAccess' => 'required',
            'lastLogin' => 'required',
            'thisAccess' => 'required',
            'thisLogin' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create([
            'userid' => $request->get('userId'),
            'password' => Hash::make($request->get('password')), 
            'name' => $request->get('name'),
            'client' => 'MLS', 
            'web_group' => 'WAREHOUSE',
            'company' => null,
            'session_id' => null,
            'email' => $request->get('email'),
            'passsword_change' => null,            
            'last_access' => $request->get('lastAccess'),
            'last_login' => $request->get('lastLogin'),
            'last_ip' => '',
            'this_access' => $request->get('thisAccess'),
            'this_login' => $request->get('thisLogin'),
            'this_ip' => '',
            'disabled' => 'N',
            'display_units' => 'HIGHEST',            
            'carrier' => null,           
        ]);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }
}
