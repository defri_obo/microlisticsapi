<?php

namespace App\Http\Controllers;

// use App\Models\StockDetailModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StockDetailController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');  
    }
    
    public function index() {
        return response()->json(['This is index of StockDetailController']);
    }

    public function show($id, Request $request) {
        try
        {
            // dd($request->client, $id, $request->site);
            DB::enableQueryLog();
            $stockDetail =  
               DB::table(DB::raw('IW_STOCK_HOLDING_V v, IS_CLIENT c'))
        ->select(DB::raw("
            NVL( TO_CHAR( v.effective_date, c.rota_format ), '-' ) AS RotaDate,
            NVL( v.batch, '-' ) AS Batch,
            NVL( v.status, '-' ) AS Disposition,
            NVL( v.ref3, '-' ) AS Ref3,
            NVL( v.ref4, '-' ) AS Ref4,
            sum( v.qty_lcd ) AS Qty,
            sum( v.weight ) AS Weight,
            sum( v.qty_hcd ) AS Pallet,
            sum( v.qty_price ) AS Price,
            v.pack_id AS Pack_ID,
            v.effective_date AS EffectiveDate
        "))
        ->where([
            'v.client'  => $request->client,
            'v.product' => $id,
            'v.site'    => $request->site,
            'v.type'      => 'H'
            ])
        ->whereRaw('v.client = c.code') 
        ->groupBy(DB::raw(" 
            NVL( TO_CHAR( v.effective_date, c.rota_format ), '-' ),
            NVL( v.batch, '-' ),
            NVL( v.status, '-' ),
            NVL( v.ref3, '-' ),
            NVL( v.ref4, '-' ),
            v.pack_id,
            v.effective_date 
        "))
        ->orderByRaw("
            v.effective_date,
            NVL( v.batch, '-' ),
            NVL( v.status, '-' ),
            NVL( v.ref3, '-' ),
            NVL( v.ref4, '-' ),
            v.pack_id
        ")
        ->get();
            // dd($stockDetail);
            // dd(DB::getQueryLog());
        }
        catch(Exception $e)
        {
            return response()->json(['message' => 'err',200]);
        }
        
        return response()->json(['data' => $stockDetail]);
    }

    public function showHeader($id, Request $request) {
        try
        {
            DB::enableQueryLog();
            $stockDetail =  DB::table('IW_STOCK_HOLDING_V v')
                            ->join('is_product p', function($join){
                                $join->on('v.client', '=', 'p.client')
                                     ->on('v.product', '=', 'p.code');
                            })
                            ->select(DB::raw("v.product as product, v.product_name as description, v.site as site, v.client as client, 
                                            p.packdesc_1 as uom, sum(v.qty_lcd) as stock_on_hand, sum(v.qty_available) as available_qty, 
                                            sum(v.qty_lcd_expected) as expected_in_qty, sum(v.qty_lcd_committed) as expected_out_qty, 
                                            case when upper(p.rotadate_type) = 'U' then 'U:Useby Date'
                                            when upper(p.rotadate_type) = 'R' then 'R:Receipt Date'
                                            else 'M:Manufacture Date'
                                            end as rotadate_type"))
            ->where('v.product', '=', $id)
            ->where('v.client', '=', $request->client)
            ->where('v.site', '=', $request->site)
            ->groupBy('v.product', 'v.client', 'v.site','v.product_name', 'p.packdesc_1', 'p.rotadate_type')
            ->get();
        }
        catch(Exception $e)
        {
            return response()->json(['message' => 'err',400]);
        }
        
        return response()->json(['data' => $stockDetail],200);
    }

    public function getForeShadowed(Request $request, $id){
        try
        {
            $client = $request->client;
            $site = $request->site;
            $product = $id;

            $result = DB::table('iw_stock_holding_v as s')
                        ->join('is_product as p', function ($join){
                            $join->on('s.product', '=', 'p.code');
                        })
                        ->selectRaw("   batch as batchNum,NVL (s.STATUS,'-') as stockStatus, sum (qty_lcd) as quantity,
                                        case when upper (p.rotadate_type) = 'U' then to_char (s.effective_date, 'YYYY-MM-dd')
                                        else to_char (s.effective_date+p.expiry_days, 'YYYY-MM-dd') end as stockExpiryDate,
                                        sum(s.weight) as weight")
                        ->where('s.client', '=' , $client)
                        ->where('s.product', '=' , $product)
                        ->where('s.site', '=' , $site)
                        ->where('s.type', '=' , 'H')
                        ->groupBy('s.batch','s.status','s.effective_date', 'p.rotadate_type', 'p.expiry_days')
                        ->orderByRaw('effective_date NULLS FIRST')
                        ->get();

            $result2 = DB::table('iw_stock_holding_v')
                         ->selectRaw("  order_no as orderNo, company as company, TO_CHAR (NVL(effective_date,sysdate),'YYYY-MM-dd') as effectiveDate,
                                        ROUND (NVL(effective_date,sysdate) - sysdate) as daysFromCR, qty_lcd as quantity, QTY_LCD_EXPECTED as qtyExpected,
                                        QTY_LCD_COMMITTED as qtyCommitted, type as type, NVL (STATUS, '-') as stockStatus, 
                                        weight as weight")
                        ->where('client', $client)
                        ->where('product', $product)
                        ->whereRaw("TYPE IN ('S','P')")
                        ->orderByRaw("effective_date NULLS FIRST, DECODE (type, 'P', 1, 'S', 2, 'H', 3)")
                        ->get();
            $opening        = 0;
            $new            = [];
            $history        = [];
            $newBalance     = [];
            $notProcessed   = [];
            $stockAdjust    = [];
            $purchaseOrder  = [];
            $salesOrder     = [];
            $openingBalance = 0;
            $closingBalance = 0;
            $stockexpirydates = null;
            $qty = 0;
            $currentDate    = Carbon::now()->format('Y-m-d');
            foreach($result as $res)
            {
                $openingBalance += $res->quantity;
                $opening += $res->quantity;
            }
            
                $plus       = 0;       
                $minus      = 0;
                $orderNoArray   = [];
                $batchArray     = [];
               
                    
                foreach($result as $res)
                {
                    $step1 = [];
                    $step2 = [];

                    $batchnum           = $res->batchnum;
                    $quantity           = $res->quantity;
                    $stockexpirydate    = new Carbon($res->stockexpirydate);
                    $stockexpirydates   = new Carbon($res->stockexpirydate);
                    $stockexpirydates   = $stockexpirydates->format('Y-m-d');
                    $stockexpirydate    = $stockexpirydate->diffInDays($currentDate);
                    $weight             = $res->weight;
                    $stockstatus        = $res->stockstatus;
                    $bn = $batchnum.$stockstatus;

                    if($quantity < 0)
                    {
                        $minus += $quantity;
                    }
                    $qty = 0;
                    $qtybf = 0;
                    $qty = $quantity;
                    foreach($result2 as $res2)
                    {
                        $orderno2           = $res2->orderno;
                        $company2           = $res2->company;
                        $effectivedate2     = $res2->effectivedate;
                        $daysfromcr2        = $res2->daysfromcr;
                        $quantity2          = $res2->quantity;
                        $in                 = $res2->qtyexpected;       
                        $out                = $res2->qtycommitted;
                        $type2              = $res2->type;
                        $stockstatus2       = $res2->stockstatus;
                        $weight2            = $res2->weight;
                        
                        if($stockstatus == $stockstatus2 && !in_array($bn, $batchArray) && !in_array($orderno2, $orderNoArray))
                        {
                            if($qty > 0 && $out > 0)
                            {
                                if($qty > 0)
                                {
                                    $qty -= $out - $minus;
                                    $minus = 0;
                                    $plus = $qty;
                                }
                                $decrease = 0;
                                $decrease = $quantity - $qty;
                                if($qty >= 0) $openingBalance -= $decrease;                        
                                if($qty < 0) 
                                {
                                    $qtybf = $qty;
                                    $minus = $qty;
                                    $qty = 0;
                                    $openingBalance -= $quantity;
                                }

                                
                                
                                $orderHistory  = [  'type'              => $stockstatus2,
                                                    'stockExpiryDate'   => $stockexpirydates,                                                    
                                                    'in'                => 0,
                                                    'out'               => $out,
                                                    'balance'           => $openingBalance,
                                                    'qty'               => $qty,
                                                    'quanti'            => $quantity,
                                                    'order No'          => $orderno2,                                        
                                ];
                                array_push($salesOrder, $orderHistory);                     
                                array_push($orderNoArray, $orderno2);                                                            
                            }

                            if($in > 0)
                            {
                                if($qty < 0 )
                                {
                                    foreach($salesOrder as $i => $his)
                                    {
                                        $currentStock = $his['qty'];
                                        if($currentStock > 0)
                                        {
                                            if($plus > 0) $minus = 0;                               
                                            $qtybf = $qty;
                                            $qty += $in + $minus + $currentStock;
                                            if($qty > 0) $plus = 0;
                                            $openingBalance += $qty;
                                            $orderHistory  = [  'type'              => $stockstatus2,
                                                                'stockExpiryDate'   => $effectivedate2,                                                    
                                                                'in'                => $in,
                                                                'out'               => 0,
                                                                'balance'           => $openingBalance,
                                                                'qty'               => $qty,
                                                                'order No'          => $orderno2                                                
                                                ];  
                                            array_push($purchaseOrder, $orderHistory);                                          
                                                
                                        } 
                                    }
                                    array_push($orderNoArray, $orderno2);
                                }
                            }
                        }
                    }
                    array_push($batchArray, $bn);
                }

                foreach($result2 as $res2)
                {
                    $orderno2           = $res2->orderno;
                    $company2           = $res2->company;
                    $effectivedate2     = $res2->effectivedate;
                    $daysfromcr2        = $res2->daysfromcr;
                    $quantity2          = $res2->quantity;
                    $in                 = $res2->qtyexpected;       
                    $out                = $res2->qtycommitted;
                    $type2              = $res2->type;
                    $stockstatus2       = $res2->stockstatus;
                    $weight2            = $res2->weight;

                    if($out > 0 && !in_array($orderno2, $orderNoArray))
                    {
                        $minus -= $out;
                        $orderHistory  = [  'type'              => $stockstatus2,
                                            'stockExpiryDate'   => $stockexpirydates,                                                    
                                            'in'                => 0, 
                                            'out'               => $out,
                                            'balance'           => $openingBalance,
                                            'qty'               => $qty,
                                            'order No'          => $orderno2                                                
                                ];
                        array_push($salesOrder, $orderHistory);
                    }
                    if($in > 0 && !in_array($orderno2, $orderNoArray))
                    {
                        $openingBalance += $in;
                        $orderHistory  = [  'type'              => $stockstatus2,
                                            'stockExpiryDate'   => $stockexpirydates,                                                    
                                            'in'                => $in, 
                                            'out'               => 0,
                                            'balance'           => $openingBalance,
                                            'qty'               => $qty,
                                            'order No'          => $orderno2                                                
                                ];
                        array_push($purchaseOrder, $orderHistory);
                    }
                }
            


            return [$salesOrder, $purchaseOrder];
        }
        catch(Exception $e)
        {
            return response()->json(['message' => 'err'],400);
        }
    }
}
