<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use DB;
use Session;
use Exception;
use JWTAuth;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('cors');
    }

    public function module(Request $request)
    {
        try
        {

            $role = $request->role;
            $menuData = DB::table('IW_MENU_ITEMS')
                    ->select('menu_id','menu_name')
                    ->where(function($query) use ($role){
                        if($role == 'warehouse_flag')
                        {
                            $query->where($role,'Y');
                        }

                        if($role == 'client_flag')
                        {
                            $query->where($role,'Y');
                        }

                        if($role == 'customer_flag')
                        {
                            $query->where($role,'Y');
                        }

                        if($role == 'supplier_flag')
                        {
                            $query->where($role,'Y');
                        }

                        if($role == 'carrier_flag')
                        {
                            $query->where($role,'Y');
                        }
                    })
                    ->get();

            $data = [];

            foreach($menuData as $menu)
            {
                array_push($data, ['menuid' => $menu->menu_id, 'menuname' => $menu->menu_name]);
            }

            return $data;

            return response()->json(['module' => $data],200);
        }
        catch(Exception $e)
        {
            return response()->json('error', 400);
        }
    }

    public function getClientAndSite(){
        $client = DB::table('is_client')->get();
        $site   = DB::table('is_site')->get();

        return response()->json(['data'=>['client' => $client,'site' => $site]],200);
    }

    protected function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password'     => 'required|unique:iw_web_user,password|max:12',
            // 'client'       => 'max:10',
            'webGroup'     => 'required|max:20',
            'company'      => 'max:10',
            'sessionId'    => 'max:64',
            'email'        => 'max:64|email',
            'lastIp'       => '',
            'thisIp'       => '',
            'disabled'     => 'max:1',
            'displayUnits' => 'max:20',
            'userId'       => 'required|unique:iw_web_user,userid|max:30',
            'carrier'      => 'max:10',
            'name'         => 'max:60|required',
            'site'         => ''
        ]);

        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first());
            return response()->json($arr);
        }

        $random_password    = Str::random(10);
        $currentTime        = Carbon::now();
        // $password           = Hash::make($request->password);
        $password           = Hash::make($random_password);
        $client             = $request->client;
        $webGroup           = $request->webGroup;
        $company            = $request->company;
        $sessionId          = $request->sessionId;
        $email              = $request->email;
        $lastAccess         = $currentTime;
        $lastLogin          = $currentTime;
        $lastIp             = $request->lastIp;
        $thisAccess         = $currentTime;
        $thisLogin          = $currentTime;
        $thisIp             = $request->thisIp;
        $disabled           = $request->disabled;
        $displayUnits       = $request->displayUnits;
        $userId             = $request->userId;
        $carrier            = $request->carrier;
        $name               = $request->name;
        $userMenu           = $request->userMenu;
        $site               = $request->site;
        $emailTo            = $email;
        $data = [
            'name'         => $name,
            'userId'       => $request->userId,
            // 'new_password' => $request->password,
            'new_password' => $random_password,
        ];
        // dd($password);
        try
        {
            $sequence        = DB::getSequence();
            $seq_IW_WEB_USER = $sequence->nextValue('IW_WEB_USER_SEQ');
            DB::beginTransaction();
            DB::table('iw_web_user')->insert([
                'web_user'          => $seq_IW_WEB_USER,
                'password'          => $password,
                'client'            => $client,
                'web_group'         => $webGroup,
                'company'           => $company,
                'session_id'        => $sessionId,
                'email'             => $email,
                'last_access'       => $lastAccess,
                'last_login'        => $lastLogin,
                'last_ip'           => $lastIp,
                'this_access'       => $thisAccess,
                'this_login'        => $thisLogin,
                'this_ip'           => $thisIp,
                'disabled'          => $disabled,
                'display_units'     => $displayUnits,
                'userid'            => $userId,
                'carrier'           => $carrier,
                'name'              => $name,
                'site'              => $site
            ]);

            $webUser = DB::table('iw_web_user')->select('web_user')->where('userid', $userId)->first();

            foreach($userMenu as $key) {
                $menus[] = ['web_user' => $webUser->web_user, 'menu_id' => $key];
            }

            if(!empty($menus)){
                DB::table('iw_user_menu')->insert($menus);
            }

            Mail::send('mailTemplates.newUserEmail', $data, function($message) use ($emailTo)
            {
                $message->to($emailTo, 'Password')
                        ->from($_ENV['MAIL_USERNAME'], 'New User Password')
                        ->subject('Microlistics');
            });

            DB::commit();
            return response()->json(['status' => 200, 'message'=> 'register successfully'], 200);
        }
        catch (\Swift_TransportException $ex) {
            DB::rollback();
            $arr = array("status" => 400, "message" => $ex->getMessage());
            return response()->json($arr);
        }
        catch(Exception $e)
        {
            DB::rollback();
            return response()->json(['status' => 400, 'message'=> $e->getMessage()], 200);
        }
        catch(Illuminate\Database\QueryException $e)
        {
            DB::rollback();
            return response()->json(['status' => 400, 'message'=> $e->getMessage()], 200);
        }
    }

    protected function updateUser(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'max:64|email',
            'disabled' => 'max:1',
            // 'client'   => 'max:3|required',
            'name'     => 'max:60|required',
            'userMenu' => 'required',
            'site'     => ''
        ]);

        if ($validator->fails()) {
             Session::flash('error', $validator->messages()->first());
             return response()->json($validator->errors()->toJson(),400);
        }

        // $password = Hash::make($request->password);
        $currentTime        = Carbon::now();
        $client             = $request->client;
        $webGroup           = $request->webGroup;
        $company            = $request->company;
        $sessionId          = $request->sessionId;
        $email              = $request->email;
        $passwordChange     = $request->passwordChange;
        if($passwordChange)
        {
            $passwordChange = Hash::make($passwordChange);
        }
        $lastAccess         = $currentTime;
        $lastLogin          = $currentTime;
        $lastIp             = $request->lastIp;
        $thisAccess         = $currentTime;
        $thisLogin          = $currentTime;
        $thisIp             = $request->thisIp;
        $disabled           = $request->disabled;
        $displayUnits       = $request->displayUnits;
        $carrier            = $request->carrier;
        $name               = $request->name;
        $userMenu           = $request->userMenu;
        $passwordChange     = $request->passwordChange;
        try
        {
            DB::beginTransaction();
            DB::table('iw_web_user')
                ->where('userid', $id)
                ->update([
                    'email'         => $email,
                    'last_access'   => $lastAccess,
                    'last_login'    => $lastLogin,
                    'client'        => $client,
                    'company'       => $company,
                    'this_access'   => $thisAccess,
                    'this_login'    => $thisLogin,
                    'disabled'      => $disabled,
                    'name'          => $name,
                    'site'          => $request->site
                ]);

            if($passwordChange)
            {
                DB::table('iw_web_user')
                    ->where('userid', $id)
                    ->update([
                        'password_change'   => Carbon::now()->subDays(1),
                        'password'          => $passwordChange
                    ]);
            }

            $webUser =  DB::table('iw_web_user')
                        ->select('web_user')
                        ->where('userid', $id)
                        ->first();

            $menuData = [];
            if($webUser)
            {
                for($i = 0 ; $i < count($userMenu) ; $i ++)
                {
                    array_push($menuData, ['web_user' => $webUser->web_user,'menu_id' => $userMenu[$i]]);
                }

                DB::table('iw_user_menu')
                    ->where('web_user', $webUser->web_user)
                    ->delete();

                DB::table('iw_user_menu')
                    ->insert($menuData);
                DB::commit();
                return response()->json(['message'=> 'updated successfully'],200);
            }
            else
            {
                DB::rollback();
                return response()->json(['message'=> 'update failed'],400);
            }

        }
        catch(Exception $e)
        {
            DB::rollback();
            return response()->json(['status' => 400, 'message'=> $e->getMessage()], 200);
        }
        catch(Illuminate\Database\QueryException $e)
        {
            DB::rollback();
            return response()->json(['status' => 400, 'message'=> $e->getMessage()], 200);
        }
    }

    public function login(Request $request){

        $userId         = $request->userid;
        $passwordLogin  = $request->password;
        $currentTime    = Carbon::now();
        try{
            $userLogin = DB::table('iw_web_user')
                           ->where('userid', '=', $userId)
                           ->where('disabled', '!=', 'Y')
                           ->first();
            if($userLogin == null){
                return response()->json(['error message'=> 'username not exists'],404);
            }

            $userLogin = (array)$userLogin;
            $password = $userLogin['password'];

            if(!Hash::check($passwordLogin, $password)){
                return response()->json(['error message'=> 'invalid username or password'],401);
            }

            DB::table('iw_web_user')
            ->where('userid', '=', $userId)
            ->update(['last_login' => $currentTime]);


            $credentials = $request->only('userid', 'password');
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $getUserModules = DB::table('iw_user_menu')
                                ->where('web_user', '=', $userLogin['web_user'])
                                ->get();

            $token                      = compact('token');
            $merges                     = (array)$token;
            $tokenValue                 = $merges['token'];
            $client                     = $userLogin['client'];
            $company                    = $userLogin['company'];
            $userLevel                  = $userLogin['web_group'];

            //user profile
            $name                       = $userLogin['name'];
            $email                      = $userLogin['email'];
            $userId                     = $userLogin['userid'];
            $lastLogin                  = $userLogin['last_login'];
            //$site                       = null;
            $site                       = $userLogin['site'];
            $webUser                    = $userLogin['web_user'];

            $result = ['client'         => $client,
                       'company'        => $company,
                       'userLevel'      => $userLevel,
                       'token'          => $tokenValue,
                       'name'           => $name,
                       'email'          => $email,
                       'userId'         => $userId,
                       'userModules'    => $getUserModules,
                       'lastLogin'      => $lastLogin,
                       'site'           => $site,
                       'webUser'        => $webUser
                      ];
                return response()->json($result,200);
        }
        catch(Exception $e){
            return response()->json(['errorMessage'=> $e->getmessage()],500);
        }
    }

    public function logout (Request $request){
        try
        {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        }
        catch(Exception $e)
        {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
            {
                return response()->json(['status' => 'Token is Invalid'],400);
            }
            else if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException)
            {
                return response()->json(['status' => 'Token is Expired'],400);
            }
            else if($e instanceof \Tymon\JWTAuth\Exceptions\JWTException)
            {
                return response()->json(['status' => 'Token is Absent'],400);
            }
            else if($e instanceof \Symfony\Component\Debug\Exception\FatalThrowableError)
            {
                return response()->json(['status' => 'Invalid Token'],400);
            }
        }
        $userId = $user->userid;
        $currentTime    = Carbon::now();
        DB::table('iw_web_user')
            ->where('userid', '=', $userId)
            ->update(['last_access' => $currentTime]);

        return response()->json(['message' => 'success'],200);
    }

    public function refreshToken(Request $request)
    {
            $token = JWTAuth::getToken();

            if(!$token)
                {
                    return response()->json(['status' => 'token is Required'],401);
                }

            try
            {
                $refreshed = JWTAuth::refresh($token);
                return response()->json(['token' => $refreshed],200);
            }
            catch(Exception $e){
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException)
                {
                    return response()->json(['status' => 'Token is Blacklisted'],401);
                }
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException)
                {
                    return response()->json(['status' => 'Token is Invalid'],400);
                }

            }
    }

    public function forgot_password(Request $request){
        $input        = $request->all();
        $rules = array(
            'web_user'     => "required",
            'email'        => "required|email",
            'new_password' => "required",
        );
        $validator           = Validator::make($input, $rules);

        $random_password     = Str::random(10);
        $web_user            = $request->web_user;
        $email               = $request->email;
        $new_password        = $random_password;
        // $new_password        = $request->new_password;
        // $hashed_new_password = Hash::make($request->new_password);
        $hashed_new_password = Hash::make($random_password);

        $user_detail = DB::table('IW_WEB_USER')
        ->select('EMAIL', 'NAME','USERID')
        ->where('WEB_USER', '=', $web_user)
        ->where('EMAIL', '=', $email)
        ->first();

        $name     = $user_detail->name;
        $emailTo  = $user_detail->email;
        $userId   = $user_detail->userid;
        $data = [
            'name'         => $name,
            'new_password' => $new_password,
            'userId'       => $userId            
        ];
        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()->first(), "data" => array()], 400);
            // $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                DB::beginTransaction();
                DB::table('iw_web_user')
                    ->where('web_user', $web_user)
                    ->where('email', $emailTo)
                    ->update([
                        'password' => $hashed_new_password
                    ]);

                DB::commit();

                Mail::send('mailTemplates.testEmail', $data, function($message) use ($emailTo)
                {
                    $message->to($emailTo, 'Password')
                            ->from($_ENV['MAIL_USERNAME'], 'Reset Password')
                            ->subject('Microlistics');
                });
                $arr = array("status" => 200, "message" => "Email has been sent!");
            } catch (\Swift_TransportException $ex) {
                // $arr = array("status" => 400, "message" => $ex->getMessage());
                DB::rollback();
                return response()->json(["status" => 400, "message" => $ex->getMessage()], 400);

            } catch (Exception $ex) {
                // $arr = array("status" => 400, "message" => $ex->getMessage());
                DB::rollback();
                return response()->json(["status" => 400, "message" => $ex->getMessage()], 400);
            }
        }
        return \Response::json($arr);
    }

    public function request_reset_password(Request $request){
        $input        = $request->all();
        $rules = array(
            'email'        => "required|email"
        );
        $validator           = Validator::make($input, $rules);
        $email               = $request->email;

        $user_detail = DB::table('IW_WEB_USER')
        ->select('EMAIL', 'NAME')
        ->where('EMAIL', '=', $email)
        ->first();

        if(!$user_detail) return \Response::json(["status" => 400, "message" => "Email not registered"],400);
        $name     = $user_detail->name;
        $emailTo  = $user_detail->email;
        $data = [
            'name'         => $name,
            'email' => $user_detail->email,
        ];
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                Mail::send('mailTemplates.resetPasswordRequest', $data, function($message) use ($emailTo)
                {
                    $message->to($_ENV['MAIL_USERNAME'], 'Request Reset Password')
                            ->from($emailTo, 'Request Reset Password')
                            ->subject('Microlistics');
                });
                $arr = array("status" => 200, "message" => "Email has been sent!");
                $stat = 200;
            } catch (\Swift_TransportException $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage());
                $stat = 400;
                DB::rollback();
            } catch (Exception $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage());
                $stat = 400;
                DB::rollback();
            }
        }
        return \Response::json($arr,$stat);
    }

    public function emailCheck(Request $request){
        $input = $request->all();
        $rules = array(
            'email' => 'required'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()->first(), "data" => array()], 400);
        }

        // DB::enableQueryLog();
        $data =
            DB::table('IW_WEB_USER')
            ->select('WEB_USER')
            ->where('EMAIL', '=', $request->email)
            ->where('DISABLED', '=', 'N')
            ->first();
        // dd(DB::getQueryLog());

        if($data){
            return response()->json(1, 200);
        } else {
            return response()->json(0, 200);
        }
    }
}
