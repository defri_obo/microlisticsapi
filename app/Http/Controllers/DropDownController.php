<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DropDownController extends Controller
{
    // public function __construct(){
    //     $this->middleware('cors');
    //     $this->middleware('loginAuth');
    //     $this->middleware('jwt.verify');
    // }

    public function getClient (Request $request)
    {
        try
        {
            $client = DB::table('is_client')
            ->select('code','name')
            ->orderBy('code','ASC')
            ->get();

            return response()->json($client,200);
        }
        catch(Exception $e)
        {
            return response()->json('something wrong',400);
        }
    }

    public function getSite (Request $request)
    {
        try
        {
            $site = DB::table('is_site')
                ->select('site', 'name')
                ->orderBy('site','ASC')
                ->get();

            return response()->json($site,200);
        }
        catch(Exception $e)
        {
            return response()->json('something wrong',400);
        }
    }

    public function getSupplier(Request $request)
    {
        try
        {
            $pdata =
                DB::table('IS_PARAM')
                ->select("pdata")
                ->where('code', '=', 'SUPPLIER_SEPRT')
                ->first();
            if($pdata->pdata === "Y" || $pdata->pdata === "YES"){
                $supplier =
                    DB::table('is_supplier_master')
                        ->select('supplier_no','name')
                        ->where('client', '=' ,$request->client)
                        ->orderBy('supplier_no','ASC')
                        ->get();
            }else{
                $supplier =
                    DB::table('is_customer_supplier_v')
                        ->select('customer_no as supplier_no','name')
                        ->where('client', '=' ,$request->client)
                        ->orderBy('customer_no','ASC')
                        ->get();
            }

            return response()->json($supplier,200);
        }
        catch(Exception $e)
        {
            return response()->json('something wrong',400);
        }
    }

    public function getOrderType(Request $request)
    {
        try
        {
            $orderType = DB::table('isis_task')
                     ->select('code','description')
                     ->where('site', '=', $request->site)
                     ->where('client', '=',$request->client)
                     ->where('genus', '=', 'RECEIVAL')
                     ->where('visible', '=', 'Y')
                     ->whereRaw("code not in (select nvl(next_task, 'no') from isis_task where site ='$request->site' and client ='$request->client')")
                     ->orderBy('code','ASC')
                     ->get();

            return response()->json($orderType,200);
        }
        catch(Exception $e)
        {
            return response()->json('something wrong',400);
        }
    }

    public function getPoOrderType()
    {
        try
        {

        }
        catch(Exception $e)
        {

        }
    }

    public function getUom(Request $request)
    {
        try
        {
            $client = $request->client;
            $product= $request->product;

            $packdesc = [];

            if (!$client)
            {
                return response()->json('client required', 400);
            }
            if(!$product)
            {
                return response()->json('product required', 400);
            }
            $uom = DB::table('is_product')
                        ->select('PACKDESC_1','PACKDESC_2','PACKDESC_3', 'PACKDESC_4', 'PACKDESC_5')
                        ->where('client', $client)
                        ->where('code', $product)
                        ->get();

            $packdesc1 = $uom[0]->packdesc_1;
            $packdesc2 = $uom[0]->packdesc_2;
            $packdesc3 = $uom[0]->packdesc_3;
            $packdesc4 = $uom[0]->packdesc_4;
            $packdesc5 = $uom[0]->packdesc_5;

            if($packdesc1 != null)
            {
                array_push($packdesc, $packdesc1);
            }
            if($packdesc4 != null)
            {
                array_push($packdesc, $packdesc4);
            }
            if($packdesc5 != null)
            {
                array_push($packdesc, $packdesc5);
            }
            if($packdesc2 != null)
            {
                array_push($packdesc,$packdesc2);
            }
            if($packdesc3 != null)
            {
                array_push($packdesc, $packdesc3);
            }

             return response()->json(['uom' => $packdesc], 200);
        }
        catch(Exception $e)
        {
            return response()->json('failed to load data', 400);
        }
    }


    public function getDisposition(Request $request)
    {
        $name = [];
        $code = [];

        try
        {
            $disposition = DB::table('is_disposition')
                                ->select('description', 'code')
                                ->orderBy('code', 'ASC')
                                ->get();
            if(count($disposition) > 0)
            {
                foreach($disposition as $dis)
                {
                    array_push($name, $dis->description);
                    array_push($code, $dis->code);
                }
            }
            return response()->json(['code' => $code, 'name' => $name], 200);
        }
        catch(Exception $e)
        {
            return response()->json('failed to load data', 400);
        }
    }

    public function getIsisTask(Request $request)
    {
        $name = [];
        $code = [];

        $site = $request->site;
        $client = $request->client;
        $order  = $request->order;

        try
        {
            $task = DB::table('isis_task')
                    ->select('code', 'description')
                    ->where('site', $site)
                    ->where('client', $client)
                    ->where(function($query) use ($order){
                        if($order == 'po') $query->where('genus', 'RECEIVAL');
                        if($order == 'so') $query->where('genus', 'PICK');
                    })
                    ->get();

            if(count($task) > 0)
            {
                foreach($task as $key)
                {
                    $cd = $key->code;
                    $nm = $key->description;
                    $nm = $cd.': '.$nm;
                    array_push($code, $key->code);
                    array_push($name, $nm);
                }
            }
            return response()->json(['code' => $code, 'name' => $name], 200);
        }

        catch(Exception $e)
        {
            return response()->json('failed to load data', 400);
        }
    }
}
