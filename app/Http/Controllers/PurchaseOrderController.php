<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
Use Exception;

class PurchaseOrderController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');
    }

    function index(Request $request) {
        $searchParam    = strtoupper($request->searchParam);
        $status         = $request->status;
        $supplier       = $request->supplier;
        $orderType      = $request->orderType;
        $site           = $request->site;
        $client         = $request->client;
        $task           = $request->task;
        $loggedClient   = $request->header('client');
        $loggedSite     = $request->header('site');
        $userLevel      = $request->header('userLevel');

        $export          = $request->export; 
        if($export=='true'){
            $num = 75000;
            //remove param page
            $request->request->remove('page');
        }else{
            $num = 50;
        }

        $pdata =
            DB::table('IS_PARAM')
            ->select("pdata")
            ->where('code', '=', 'SUPPLIER_SEPRT')
            ->first();
        $pdata = $pdata->pdata;

        if($pdata == "N" || $pdata == "No"){
            $purchaseOrder =
                DB::table("IW_PO_HEAD_V r")
                ->leftJoin("is_customer_supplier_v t", function($join){
                    $join->on("r.client", "t.client")
                            ->on("t.customer_no","r.supplier");
            });
        }else{
            $purchaseOrder =
                DB::table('IW_PO_HEAD_V r')
                ->leftJoin('is_supplier_master t', function($join){
                    $join->on('r.client', 't.client')
                            ->on('r.supplier', 't.customer_no');
            });
        }

        $purchaseOrderResult = $purchaseOrder
            ->select(
                "r.SITE as SITE",
                "r.CLIENT as CLIENT",
                "r.ORDER_NO as ORDER_NO",
                "r.ORDER_TYPE as ORDER_TYPE",
                "r.ISIS_TASK as ISIS_TASK",
                "r.SUPPLIER as SUPPLIER_NO",
                "t.NAME as SUPPLIER_NAME",
                "r.STATUS_DESC as STATUS",
                "r.DATE_DUE as DELIVERY_DATE",
                "r.DATE_RECD as DATE_RECEIVED",
                "r.DATE_RELEASED as DATE_RELEASED",
                "r.DATE_COMPLETED as DATE_COMPLETED",


                "r.CUSTOMER_PO_NO as CUSTOMER_ORDER_REF",
                "r.VENDOR_ORD_NO as VENDOR_ORD_REF"
            )
            ->where(function($query) use ($searchParam){
                if($searchParam)
                {
                    $query->where('r.ORDER_NO', '=', $searchParam)
                            ->orWhere('r.SITE', '=', $searchParam)
                            ->orWhere('r.CLIENT', '=', $searchParam);
                }
            })
            ->where(function($query) use ($loggedSite){ //site from logged user
                if($loggedSite)
                {
                    $query->where('r.SITE', '=', $loggedSite);
                }
            })
            ->where(function($query) use ($site){ //site from filter
                if(isset($site) && $site !== 'all')
                {
                    $query->where('r.SITE', '=', $site);
                }
            })
            ->where(function($query) use($task){
                if($task && $task !== 'All') $query->where('r.isis_task', $task);
            })
            ->where(function($query) use ($client){ //client from filter
                if(isset($client) && $client !== 'all' )
                {
                    $query->where('r.client', '=', $client);
                }
            })
            ->where(function($query) use ($loggedClient){ //client from logged user
                if($loggedClient)
                {
                    $query->where('r.client', '=', $loggedClient);
                }
            })
            ->where(function($query) use ($status){
                if($status == 'open')
                {
                    $query->where('r.STATUS_DESC', 'not like', '4%');
                }
                else if($status == 'released')
                {
                    $query->where('r.STATUS_DESC', 'like', '2%');
                }
                else if($status == 'part_released')
                {
                    $query->where('r.STATUS_DESC', 'like', '3%');
                }
                else if($status == 'available')
                {
                    $query->where('r.STATUS_DESC', 'like', '1%');
                }
                else if($status == 'completed')
                {
                    $query->where('r.STATUS_DESC', 'like', '4%');
                }
                else if($status == 'unavailable')
                {
                    $query->where('r.STATUS_DESC', 'like', '0%');
                }
                else if($status == 'all')
                {
                    return $query;
                }
            })
            ->where(function($query) use ($orderType){
                 if($orderType=="(NULL)" && isset($orderType))
                {
                    $query  ->where('r.ORDER_TYPE', '=', null);
                }elseif($orderType !== "all" && isset($orderType)){
                    $query  ->where('r.ORDER_TYPE', '=', $orderType);
                }
            })
            ->orderBy('r.site', 'asc')
            ->distinct()->paginate($num);
        return response()->json(['data' => $purchaseOrderResult], 200);
    }

    public function purchaseOrderDetail(Request $request,$site, $client, $product)
    {
        $export          = $request->export; 
        if($export=='true'){
            $num = 75000;
            //remove param page
            $request->request->remove('page');
        }else{
            $num = 50;
        }

        try
        {
            $result = DB::table('IW_PO_HEAD_V head')
                    ->select(
                        "foot.site AS SITE",
                        "head.Order_No AS ORDER_NO",
                        "foot.CLIENT AS CLIENT",
                        "client.NAME AS CLIENT_NAME",
                        "head.STATUS AS STATUS",
                        "head.STATUS_DESC AS STATUS_DESCRIPTION",
                        "head.ORDER_TYPE AS ORDER_TYPE",
                        "head.ISIS_TASK as ISIS_TASK",
                        "head.SUPPLIER AS SUPPLIER_ID",
                        "head.SHIP_TO_NAME AS SUPPLIER_NAME",
                        "head.CUSTOMER_PO_NO AS CUSTOMER_ORDER_REF",
                        "head.VENDOR_ORD_NO AS VENDOR_ORD_REF",
                        "head.DATE_DUE AS DELIVERY_DATE",
                        "head.DATE_RECD AS DATE_RECEIVED",
                        "head.DATE_RELEASED AS DATE_RELEASED",
                        "head.DATE_COMPLETED AS DATE_COMPLETED",
                        "foot.ORIG_LINE_NUMBER AS ORIG_LINE_NUMBER",
                        "foot.PRODUCT AS PRODUCT",
                        "foot.PRODUCT_NAME AS PRODUCT_NAME",
                        "foot.QTY_LCD AS QUANTITY",
                        "foot.WEIGHT AS WEIGHT",
                        "foot.PACKDESC_1 AS PACKDESC_1",
                        "foot.PACKDESC_2 AS PACKDESC_2",
                        "foot.COMPLETED AS COMPLETED",
                        "foot.DISPOSITION AS DISPOSITION",
                        "foot.REF3 AS REF3",
                        "foot.BATCH AS BATCH",
                        "foot.ROTA1 AS ROTADATE",
                        "foot.WGT_PROCESSED AS WEIGHT_PROCESSED",
                        "foot.QTY_PROCESSED AS QTY_PROCESSED",
                        "foot.REF4 AS REF4" )
                    ->join('IW_PO_LINE_V as foot', 'head.order_no', '=' , 'foot.order_no')
                    ->join('IS_CLIENT as client', 'client.code', '=' , 'foot.client')
                    ->where('head.order_no','=', $product)
                    ->where('head.client','=', $client)
                    ->where('foot.site','=', $site)
                    ->orderBy('foot.ORIG_LINE_NUMBER','ASC')
                    ->paginate($num);
                    return response()->json(['data' =>$result], 200);
        }
        catch(Exception $e)
        {
            return response()->json(['error' => 'something wrong'], 400);
        }
    }

    public function store(Request $request)
    {
        $datas                      = $request->all();
        $sequence                   = DB::getSequence();
        $seq_IS_IFAC                = $sequence->nextValue('IS_IFAC_SEQ');
        $orderDetails               = $datas['orderDetails'];
        $lineDetails                = $datas['lineDetails'];
        $web_user                   = $request->web_user;

        // $seq_IS_IFAC                = DB::table('IS_IFAC')->select('ifac_key', 'packet_number')->orderBy('ifac_key', 'desc')->first();
        // // $next_IFAC_KEY_IS_IFAC      = $seq_IS_IFAC->ifac_key+1;
        // $next_PACKET_NUMBER_IS_IFAC = $seq_IS_IFAC->packet_number+1;
        // $next_IFAC_KEY_IS_IFAC_OR1  = $seq_IS_IFAC_OR1->ifac_key+1;
        // $next_IFAC_KEY_IS_IFAC_OR2  = $seq_IS_IFAC_OR2->ifac_key+1;
        $now                        = Carbon::now();

        $input = $request->all();
        $rules = array(
            'orderDetails.*.site'              => 'required',
            'orderDetails.*.client'            => 'required',
            // 'orderDetails.*.customerOrderRef'  => 'numeric',
            'orderDetails.*.orderType'         => 'required',
            'orderDetails.*.orderNo'           => 'required',
            'orderDetails.*.orderDate'         => 'required',
            // 'orderDetails.*.vendorOrderRef'    => 'required',
            'orderDetails.*.web_user'          => 'required',

            // 'lineDetails.*.lineNumber'         => 'required|numeric',
            'lineDetails.*.product'            => 'required',
            // 'lineDetails.*.productDescription' => 'required',
            'lineDetails.*.qty'                => 'required|numeric',
            'lineDetails.*.uom'                => 'required',
            // 'lineDetails.*.rotadate'           => 'required',
            // 'lineDetails.*.batch'              => 'required',
            // 'lineDetails.*.ref3'               => 'required',
            // 'lineDetails.*.ref4'               => 'required',
            // 'lineDetails.*.disposition'        => 'required',
            // 'lineDetails.*.weight'             => 'required|numeric'
        );

        $validator           = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first());
            return \Response::json($arr);
        }


        #get supplier code & customer dode
        $supplier_code = null;
        $customer_code = null;
        $user_data     = DB::table('IW_WEB_USER')->where('web_user', $orderDetails[0]['web_user'])->first();
        if(empty($user_data)){
            return response()->json(['message'=> 'User not found'], 400);
        }

        // if(!empty($user_data->company)){
        //     $supplier_code = $user_data->company;
        //     $customer_code = $user_data->company;
        // }else{
        //     //get supplier
        //     $supplier_master = DB::table('IS_SUPPLIER_MASTER')->where('client', $user_data->client)->first();
        //     if(!empty($supplier_master)){
        //         $supplier_code = $supplier_master->supplier_no;
        //     }

        //     //get customer code
        //     $customer_supplier = DB::table('IS_CUSTOMER_SUPPLIER_V')->where('client', $user_data->client)->first();
        //     if(!empty($customer_supplier)){
        //         $customer_code = $customer_supplier->customer_no;
        //     }
        // }

        //Order Details
        $client             = null;
        $customerOrderRef   = null;
        $orderDate          = null;
        $orderNo            = null;
        $orderType          = null;
        $site               = null;
        $supplier           = null;
        $vendorOrderRef     = null;

        if(isset($orderDetails[0]['client']))           $client             = $orderDetails[0]['client'];
        if(isset($orderDetails[0]['customerOrderRef'])) $customerOrderRef   = $orderDetails[0]['customerOrderRef'];
        if(isset($orderDetails[0]['orderDate']))        $orderDate          = $orderDetails[0]['orderDate'];
        if(isset($orderDetails[0]['orderNo']))          $orderNo            = $orderDetails[0]['orderNo'];
        if(isset($orderDetails[0]['orderType']))        $orderType          = $orderDetails[0]['orderType'];
        if(isset($orderDetails[0]['site']))             $site               = $orderDetails[0]['site'];
        if(isset($orderDetails[0]['supplier']))         $supplier           = $orderDetails[0]['supplier'];
        if(isset($orderDetails[0]['vendorOrderRef']))   $vendorOrderRef     = $orderDetails[0]['vendorOrderRef'];

        $order_to_IS_IFAC = [
            // 'ifac_key'                  => $next_IFAC_KEY_IS_IFAC,
            'ifac_key'                  => intval($seq_IS_IFAC),
            'client'                    => $client,
            'stream_id'                 => 'OR',
            'packet_number'             => intval($seq_IS_IFAC),
            // 'packet_number'             => intval($seq_IS_IFAC),
            'record_number'             => 1,
            'table_level'               => 0,
            'transmit_indicator'        => 'SN',
            'rejection_reason'          => null,
            'rejection_status'          => null,
            'process_status'            => 'P',
            'transfer_date'             => $now,
            'order_no'                  => $orderNo
        ];


        $order_to_IS_IFAC_OR1 = [
            'ifac_key'           => intval($seq_IS_IFAC),
            // 'ifac_key'           => 'IS_IFAC_SEQ.nextval',
            'site'               => $site,
            'customer_number'    => $supplier,
            // 'customer_number'      => $customer_code,
            'supplier_number'    => $supplier,
            'customer_po_number' => $customerOrderRef,
            'order_type'         => $orderType,
            'cust_order_number'  => $orderNo,
            'order_date'         => $orderDate,
            'vendor_ord_no'      => $vendorOrderRef,
            'action_indicator'   => 'A',
            'isis_task'          => null
        ];

        $line = array();
        $order_to_IS_IFAC2 = [];

        foreach($lineDetails as $lineDetail){
            $seq_IS_IFAC2       = $sequence->nextValue('IS_IFAC_SEQ');
            $batch              = null;
            $disposition        = null;
            $lineNumber         = null;
            $orderDate          = null;
            $product            = null;
            $productDescription = null;
            $qty                = null;
            $ref3               = null;
            $ref4               = null;
            $rotadate           = null;
            $uom                = null;
            $weight             = null;

            if(isset($lineDetail['batch']))                 $batch              = $lineDetail['batch'];
            if(isset($lineDetail['disposition']))           $disposition        = $lineDetail['disposition'];
            if(isset($lineDetail['lineNumber']))            $lineNumber         = $lineDetail['lineNumber'];
            if(isset($lineDetail['orderDate']))             $orderDate          = $lineDetail['orderDate'];
            if(isset($lineDetail['product']))               $product            = $lineDetail['product'];
            if(isset($lineDetail['productDescription']))    $productDescription = $lineDetail['productDescription'];
            if(isset($lineDetail['qty']))                   $qty                = $lineDetail['qty'];
            if(isset($lineDetail['ref3']))                  $ref3               = $lineDetail['ref3'];
            if(isset($lineDetail['ref4']))                  $ref4               = $lineDetail['ref4'];
            if(isset($lineDetail['rotadate']))              $rotadate           = $lineDetail['rotadate'];
            if(isset($lineDetail['uom']))                   $uom                = $lineDetail['uom'];
            if(isset($lineDetail['weight']))                $weight             = $lineDetail['weight'];

            $order_to_IS_IFAC2[] = [
                'ifac_key'                  => $seq_IS_IFAC2,
                // 'ifac_key'                  => $next_IFAC_KEY_IS_IFAC++,
                'client'                    => $client,
                'stream_id'                 => 'OR',
                'packet_number'             => intval($seq_IS_IFAC),
                'record_number'             => 1,
                'table_level'               => 1,
                'transmit_indicator'        => 'SN',
                'rejection_reason'          => null,
                'rejection_status'          => null,
                'process_status'            => 'P',
                'transfer_date'             => $now,
                'order_no'                  => $orderNo
            ];

            $line[] = array(
                'ifac_key'         => intval($seq_IS_IFAC2),
                // 'ifac_key'         => $next_IFAC_KEY_IS_IFAC_OR2++,
                // 'ifac_key'         => 'IS_IFAC_SEQ.nextval',
                'line_number'      => intval($lineNumber),
                'product'          => $product,
                'quantity_ordered' => intval($qty),
                'rotadate'         => $rotadate,
                'ref'              => $batch,
                'ref3'             => $ref3,
                'ref4'             => $ref4,
                'uom'              => $uom,
                'disposition'      => $disposition,
                'weight'           => $weight,
                'handling'         => 'Q',
                'action_indicator' => 'A',
                'cust_order_number'  => $orderNo
            );
        }

        try {
            DB::beginTransaction();
            DB::table('is_ifac')->insert($order_to_IS_IFAC);
            DB::table('is_ifac_or1')->insert($order_to_IS_IFAC_OR1);
            DB::table('is_ifac')->insert($order_to_IS_IFAC2);
            DB::table('Is_Ifac_or2')->insert($line);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            return response()->json(['message'=> $ex->getMessage()], 400);
        }
        return response()->json(['message'=> 'Successfully added'], 200);
    }


    public function getPoResourceCreation(Request $request)
    {
        try
        {
            $company = $request->company;
            $client  = $request->client;
            if($company)
            {
                $supplier = DB::table('is_supplier_master')
                        ->select('name')
                        ->where('supplier_no', $company)
                        ->get();
            }
            else
            {
                $supplier = DB::table('is_supplier_master')
                         ->select('name', 'supplier_no')
                         ->where('client', $client)
                         ->get();
            }

            $site = DB::table('is_site')
                      ->select('site','name')
                      ->orderBy('name','asc')
                      ->get();

            $orderType = DB::table('is_code_description')
                           ->select('code', 'description')
                           ->where('type_key', 'PO_ORDER_TYPE')
                           ->get();

            $orderTypeFilter  = [];
            $getOrderTypeFilter = DB::table('iw_po_head_v')
                                ->select('order_type')
                                ->distinct()
                                ->orderBy('order_type','asc')
                                ->get();
             if(!empty($getOrderTypeFilter)){
                foreach ($getOrderTypeFilter as $row) {
                    $tmp = [];
                    if(isset($row->order_type)){
                        $tmp['code'] = $row->order_type;
                        $tmp['description'] = $row->order_type;
                    }else{
                        $tmp['code'] = "(NULL)";
                        $tmp['description'] = "(NULL)";
                    }
                    array_push($orderTypeFilter, $tmp);
                }
            }

            return response()->json([ 'supplier' => $supplier, 'site' => $site, 'orderType' => $orderType,'orderTypeFilter' => $orderTypeFilter ],200);
        }
        catch(Exception $e)
        {
            return response()->json(['message' => 'failed to load'],400);
        }
    }

}

