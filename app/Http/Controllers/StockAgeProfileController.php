<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockAgeProfileController extends Controller
{
    public function __construct(){
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');
        $this->middleware('cors');
    }

    public function searchFilterStockAgeProfile(Request $request)
    {
        try
        {
            $searchParam    = $request->searchParam;
            $site           = $request->site;
            $client         = $request->client;
            $searchAndFilterStockAgeProfile = DB::table('iw_stock_age_profile_v v')
                                                ->join('is_product t', function($join){
                                                    $join->on('v.client', 't.client')
                                                        ->on('v.product', 't.code');
                                                })
                                                ->join('iw_stock_holding_v s', function($join){
                                                        $join->on('s.product', 't.code')
                                                            ->on('s.client', 't.client');
                                                })
                                                ->selectRaw('DISTINCT
                                                            v.site,
                                                            v.client,
                                                            v.product,
                                                            v.product_name,
                                                            t.packdesc_1,
                                                            t.packdesc_2,
                                                            t.shelf_life,
                                                            s.qty_lcd as On_Hand,
                                                            s.qty_lcd_expected as Expected_In,
                                                            s.qty_lcd_committed as Expected_Out')
                                                ->where(function($query) use ($site)
                                                {
                                                    if($site && $site !== 'all')
                                                    {
                                                        $query->where('v.site', '=', $site);
                                                    }
                                                })
                                                ->where(function($query) use ($client)
                                                {
                                                    if($client && $client !== 'all')
                                                    {
                                                        $query->where('v.client', '=', $client);
                                                    }
                                                })
                                                ->where(function($query) use ($searchParam)
                                                {
                                                    if($searchParam)
                                                    {
                                                        $query->where('v.product_name', 'like', '%' . $searchParam . '%')
                                                              ->orWhere('v.product', 'like', '%' . $searchParam . '%');
                                                    }
                                                })
                                                ->paginate(50);
                                                return response()->json($searchAndFilterStockAgeProfile, 200);
        }
        catch(Exception $e){
                return response()->json(['error'=>$e], 404);
        }
    }
}
