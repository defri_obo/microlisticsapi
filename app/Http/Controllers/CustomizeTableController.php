<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDO;
use Yajra\Pdo\Oci8\Exceptions\Oci8Exception;
use Illuminate \ Database \ QueryException;

class CustomizeTableController extends Controller
{

    public function customizeTable(Request $request){

        //Check the user level
        if($request->userLevel != 'SUPERADMIN')
        {
            return 'Please contact super admin to use this module';
        }

        if($request->id == null && $request->customizeColumnName != null)
        {
            return $this->insertColumn($request);
            // return 'insert';
        }
        else if ($request->id != null && $request->customizeColumnName != null)
        {
            return $this->updateColumn($request);
            // return 'update';
        }
        else if ($request->id != null && $request->customizeColumnName == null)
        {
            return $this->deleteColumn($request);
            // return 'delete';
        }

        else if ($request->tableName != null && $request->param1 != null)
        {
            return $this->createTable($request);
        }
        else
        {
            return 'retrieve tableid by module';
        }
    }

    protected function insertColumn(Request $request)
    {
        $procedureName = 'InsertCustomizeTable';
        $bindings = [
            'param1' => $request->defaultClientCode,
            'param2' => $request->userLevel,
            'param3' => $request->tableName,
            'param4' => $request->columnName,
            'param5' => $request->customizeColumnName,
            'param6' => $request->userName,
        ];
        try
        {
            DB::executeProcedure($procedureName, $bindings);
        }
        catch(Oci8Exception $e)
        {
            return "insert failed. check your value again";
        }
        
        return "insert success";
    }

    protected function updateColumn(Request $request)
    {
        $procedureName = 'UpdateCustomizeTable';
        $bindings = [
            'param1' => $request->defaultClientCode,
            'param2' => $request->tableName,
            'param3' => $request->columnName,
            'param4' => $request->customizeColumnName,
            'param5' => $request->userName,
            'param6' => $request->id,
        ];
        try
        {
            DB::executeProcedure($procedureName, $bindings);
        }
        catch(Oci8Exception $e)
        {
            return "update failed. check your value agaaain";
        }
        
        return "update success";
    }

    protected function deleteColumn(Request $request)
    {
        $procedureName = 'DeleteCustomizeTable';
        $bindings = [
            'param1' => $request->defaultClientCode,
            'param3' => $request->tableName,
            'param4' => $request->columnName,
            'param6' => $request->userName,
            'param8' => $request->id,
        ];
        try
        {
            DB::executeProcedure($procedureName, $bindings);
        }
        catch(Oci8Exception $e)
        {
            return "delete failed. check your value agaaain";
        }
        
        return "delete success";
    }

    protected function createTable(Request $request)
    {
        $procedureName = 'CreateClientTable';
        $bindings = [
            'param1' => $request->tableName,
            'param2' => $request->param1,
            'param3' => $request->param2,
            'param4' => $request->param3,
        ];

        try
        {
            DB::executeProcedure($procedureName, $bindings);
        }
        catch(Oci8Exception $e)
        {
            return 'create table failed';
        }

        return 'table created successfully';
    }

    protected function select(){

        $param = 'product';
        $a = DB::table('iw_stock_holding_v')->select($param)->get();
        return $a;
    }

    protected function selectTable(){
        // $table = DB::select(DB::raw("SELECT column_name FROM ALL_TAB_COLUMNS WHERE lower(table_name) = 'customize_client_table'"));
        $table = DB::table('customize_client_table')->get();
        $tableArray = [];

        foreach($table as $column)
        {
            array_push($tableArray, $column->table_name.'.'.$column->column_name);
        }

        $a = DB::table('iw_stock_holding_v')
                ->join('is_stock_v', 'iw_stock_holding_v.product', '=' , 'is_stock_v.product')
                ->select($tableArray)->get();
        return $a;
    }

    protected function transactions(){
        $procedureName = 'obo_transactions';

        try
        {
            DB::beginTransaction();
            DB::executeProcedure($procedureName);
            DB::commit();
            return 'success';
        }
        catch(Oci8Exception $e)
        {
            DB::rollBack();
            return 'create table failed';
        }
    }

    protected function testes(Request $request)
    {
        // $arr = ['asasdasdsaadd','asdassada','asdasdassadasd'];
        // $date2 = date('Y-m-d', strtotime("+6 day", strtotime($startDate)));
        // $procedureName = 'sp_tes';
        // $bindings = [
        //     'param1' =>  implode(', ', $arr)
        // ];
        //     DB::executeProcedure($procedureName, $bindings);
        // return "insert success";

        try{
            DB::beginTransaction();

        DB::table('usrs')->insert(['userid' => 'ajdf;nyunyuu' , 'password' => 'nyuunyuu']);
        DB::table('usrs1')->insert(['userid' => 'ajdf;' , 'password' => 'sdf']);
        DB::commit();
        return 'success';
        }
        catch(Exception $e)
        {
            DB::rollback();
            return 'fail';
        }
    }
}
