<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Exception;

class RenameColumnController extends Controller
{
    public function __construct() {
        // $this->middleware('cors');
        // $this->middleware('loginAuth');
        // $this->middleware('jwt.verify');  
    }
    
    public function index() { 
        return response()->json(['data' => 'index']);
    }

    public function getStockholdingColumn(Request $request) {
        try {
            /* 
            get file from specific path
            ie: /var/www/laravel/app/storage/column/stockholding/filename.json
            */
            $path = storage_path()."/column/stockholding/".strtolower($request->client).".json"; 
            $arr  = json_decode(file_get_contents($path), true);

            return response()->json(['data' => $arr], 200);
        } catch (Exception $ex) {
            return response()->json(["status" => 400, "message" => "Client Not Found!"], 400);
        }
    }

    public function putStockholdingColumn(Request $request) {
        // validate request input
        $validator = Validator::make($request->all(), [
            'SITE'             => 'required',
            'CLIENT'           => 'required',
            'PRODUCT'          => 'required',
            'DESCRIPTION'      => 'required',
            'DISPOSITION'      => 'required',
            'UOM'              => 'required',
            'STATUS'           => 'required',
            'STOCK_ON_HAND'    => 'required',
            'ON_HAND_WGT'      => 'required',
            'EXPECTED_IN_QTY'  => 'required',
            'EXPECTED_IN_WGT'  => 'required',
            'EXPECTED_OUT_QTY' => 'required',
            'PRICE'            => 'required',
            'PALLETS'          => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()->first()], 400);
        }

        if (!$request->input('client')) {
            return response()->json(["status" => 400, "message" => "Client Is Required!"], 400);
        }

        // mapping request input into required format
        $data = [
            [
                'SITE'             => $request->SITE,
                'CLIENT'           => $request->CLIENT,
                'PRODUCT'          => $request->PRODUCT,
                'DESCRIPTION'      => $request->DESCRIPTION,
                'DISPOSITION'      => $request->DISPOSITION,
                'UOM'              => $request->UOM,
                'STATUS'           => $request->STATUS,
                'STOCK ON HAND'    => $request->STOCK_ON_HAND,
                'ON HAND WGT'      => $request->ON_HAND_WGT,
                'EXPECTED IN QTY'  => $request->EXPECTED_IN_QTY,
                'EXPECTED IN WGT'  => $request->EXPECTED_IN_WGT,
                'EXPECTED OUT QTY' => $request->EXPECTED_OUT_QTY,
                'PRICE'            => $request->PRICE,
                'PALLETS'          => $request->PALLETS
            ]
        ];
        
        /* 
        put file into specific path
        ie: /var/www/laravel/app/storage/column/stockholding/filename.json
        */
        Storage::put("/stockholding/".strtolower($request->input('client')).".json", json_encode($data));

        return response()->json(["status" => 200, "message" => "Config Saved!"], 200);
    }

    public function getSalesOrderColumn(Request $request) {
        try {
            /* 
            get file from specific path
            ie: /var/www/laravel/app/storage/column/salesOrder/filename.json
            */
            $path = storage_path()."/column/salesOrder/".strtolower($request->client).".json"; 
            $arr  = json_decode(file_get_contents($path), true);

            return response()->json(['data' => $arr], 200);
        } catch (Exception $ex) {
            return response()->json(["status" => 400, "message" => "Client Not Found!"], 400);
        }
    }



    public function putSalesOrderColumn(Request $request) {
        // validate request input
        $validator = Validator::make($request->all(), [
            'SITE'               => 'required',
            'CLIENT'             => 'required',
            'ORDER_NO'           => 'required',
            'ORDER_TYPE'         => 'required',
            'TASK'               => 'required',
            'CUSTOMER_NO'        => 'required',
            'CUSTOMER_NAME'      => 'required',
            'STATUS'             => 'required',
            'DELIVERY_DATE'      => 'required',
            'DATE_RECEIVED'      => 'required',
            'DATE_RELEASED'      => 'required',
            'DATE_COMPLETED'     => 'required',
            'CUSTOMER_ORDER_REF' => 'required',
            'VENDOR_ORDER_NO'    => 'required',
            'ADDRESS1'           => 'required',
            'ADDRESS2'           => 'required',
            'ADDRESS3'           => 'required',
            'ADDRESS4'           => 'required',
            'ADDRESS5'           => 'required',
            'SUBURB'             => 'required',
            'POSTCODE'           => 'required',
            'STATE'              => 'required',
            'COUNTRY'            => 'required',
            'LOAD_NUMBER'        => 'required',
            'LOAD_START'         => 'required',
            'LOAD_FINISH'        => 'required',
            'CONSIGNMENT_NO'     => 'required',
            'FREIGHT_CHARGE'     => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()->first()], 400);
        }

        if (!$request->input('client')) {
            return response()->json(["status" => 400, "message" => "Client Is Required!"], 400);
        }

        // mapping request input into required format
        $data = [
            [
                'SITE'               => $request->SITE,
                'CLIENT'             => $request->CLIENT,
                'ORDER NO'           => $request->ORDER_NO,
                'ORDER TYPE'         => $request->ORDER_TYPE,
                'TASK'               => $request->TASK,
                'CUSTOMER NO'        => $request->CUSTOMER_NO,
                'CUSTOMER NAME'      => $request->CUSTOMER_NAME,
                'STATUS'             => $request->STATUS,
                'DELIVERY DATE'      => $request->DELIVERY_DATE,
                'DATE RECEIVED'      => $request->DATE_RECEIVED,
                'DATE RELEASED'      => $request->DATE_RELEASED,
                'DATE COMPLETED'     => $request->DATE_COMPLETED,
                'CUSTOMER ORDER REF' => $request->CUSTOMER_ORDER_REF,
                'VENDOR ORDER NO'    => $request->VENDOR_ORDER_NO,
                'ADDRESS1'           => $request->ADDRESS1,
                'ADDRESS2'           => $request->ADDRESS2,
                'ADDRESS3'           => $request->ADDRESS3,
                'ADDRESS4'           => $request->ADDRESS4,
                'ADDRESS5'           => $request->ADDRESS5,
                'SUBURB'             => $request->SUBURB,
                'POSTCODE'           => $request->POSTCODE,
                'STATE'              => $request->STATE,
                'COUNTRY'            => $request->COUNTRY,
                'LOAD NUMBER'        => $request->LOAD_NUMBER,
                'LOAD START'         => $request->LOAD_START,
                'LOAD FINISH'        => $request->LOAD_FINISH,
                'CONSIGNMENT NO'     => $request->CONSIGNMENT_NO,
                'FREIGHT CHARGE'     => $request->FREIGHT_CHARGE
            ]
        ];
        
        /* 
        put file into specific path
        ie: /var/www/laravel/app/storage/column/stockholding/filename.json
        */
        Storage::put("/salesOrder/".strtolower($request->input('client')).".json", json_encode($data));

        return response()->json(["status" => 200, "message" => "Config Saved!"], 200);
    }

     public function getSalesOrderDetailColumn(Request $request) {
        try {
            /* 
            get file from specific path
            ie: /var/www/laravel/app/storage/column/salesOrder/filename.json
            */
            $path = storage_path()."/column/salesOrderDetail/".strtolower($request->client).".json"; 
            $arr  = json_decode(file_get_contents($path), true);

            return response()->json(['data' => $arr], 200);
        } catch (Exception $ex) {
            return response()->json(["status" => 400, "message" => "Client Not Found!"], 400);
        }
    }

    

    public function putSalesOrderDetailColumn(Request $request) {
        // validate request input
        $validator = Validator::make($request->all(), [
            'LINE_NO'         => 'required',
            'PRODUCT'         => 'required',
            'DESCRIPTION'     => 'required',
            'QTY'             => 'required',
            'UOM'             => 'required',
            'QTY_PROCESSED'   => 'required',
            'WEIGHT'          => 'required',
            'WEIGHT_PROCESSED' => 'required',
            'COMPLETED'       => 'required',
            'OOS'             => 'required',
            'RELEASED'        => 'required',
            'BATCH'           => 'required',
            'REF2'            => 'required',
            'REF3'            => 'required',
            'REF4'            => 'required',
            'DISPOSITION'     => 'required',
            'PACK_ID'         => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()->first()], 400);
        }

        if (!$request->input('client')) {
            return response()->json(["status" => 400, "message" => "Client Is Required!"], 400);
        }

        // mapping request input into required format
        $data = [
            [ 
                'LINE NO'         => $request->LINE_NO,
                'PRODUCT'         => $request->PRODUCT,
                'DESCRIPTION'     => $request->DESCRIPTION,
                'QTY'             => $request->QTY,
                'UOM'             => $request->UOM,
                'QTY PROCESSED'   => $request->QTY_PROCESSED,
                'WEIGHT'          => $request->WEIGHT,
                'WEIGHT PROCESSED' => $request->WEIGHT_PROCESSED,
                'COMPLETED'       => $request->COMPLETED,
                'OOS'             => $request->OOS,
                'RELEASED'        => $request->RELEASED,
                'BATCH'           => $request->BATCH,
                'REF2'            => $request->REF2,
                'REF3'            => $request->REF3,
                'REF4'            => $request->REF4,
                'DISPOSITION'     => $request->DISPOSITION,
                'PACK ID'         => $request->PACK_ID
            ]
        ];
        
        /* 
        put file into specific path
        ie: /var/www/laravel/app/storage/column/stockholding/filename.json
        */
        Storage::put("/salesOrderDetail/".strtolower($request->input('client')).".json", json_encode($data));

        return response()->json(["status" => 200, "message" => "Config Saved!"], 200);
    }

    public function getPurchaseOrderColumn(Request $request) {
        try {
            /* 
            get file from specific path
            ie: /var/www/laravel/app/storage/column/stockholding/filename.json
            */
            $path = storage_path()."/column/purchaseOrder/".strtolower($request->client).".json"; 
            $arr  = json_decode(file_get_contents($path), true);

            return response()->json(['data' => $arr], 200);
        } catch (Exception $ex) {
            return response()->json(["status" => 400, "message" => "Client Not Found!"], 400);
        }
    }

    public function putPurchaseOrderColumn(Request $request) { 
        // validate request input
        $validator = Validator::make($request->all(), [
            'SITE'              => 'required',
            'CLIENT'            => 'required',
            'ORDER_NO'          => 'required',
            'ORDER_TYPE'        => 'required',
            'TASK'              => 'required',
            'SUPPLIER_NO'       => 'required',
            'SUPPLIER_NAME'     => 'required',
            'STATUS'            => 'required',
            'ORDER_DATE'        => 'required',
            'DATE_RECEIVED'     => 'required',
            'DATE_RELEASED'     => 'required',
            'DATE_COMPLETED'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()], 400);
        }

        if (!$request->input('client')) {
            return response()->json(["status" => 400, "message" => "Client Is Required!"], 400);
        }

        // mapping request input into required format
        $data = [
            [ 
                'SITE'              => $request->SITE,
                'CLIENT'            => $request->CLIENT,
                'ORDER NO'          => $request->ORDER_NO,
                'ORDER TYPE'        => $request->ORDER_TYPE,
                'TASK'              => $request->TASK,
                'SUPPLIER NO'       => $request->SUPPLIER_NO,
                'SUPPLIER NAME'     => $request->SUPPLIER_NAME,
                'STATUS'            => $request->STATUS,
                'ORDER DATE'        => $request->ORDER_DATE,
                'DATE RECEIVED'     => $request->DATE_RECEIVED,
                'DATE RELEASED'     => $request->DATE_RELEASED,
                'DATE COMPLETED'    => $request->DATE_COMPLETED
            ]
        ];
        
        /* 
        put file into specific path
        ie: /var/www/laravel/app/storage/column/stockholding/filename.json
        */
        Storage::put("/purchaseOrder/".strtolower($request->input('client')).".json", json_encode($data));

        return response()->json(["status" => 200, "message" => "Config Saved!"], 200);
    } 

    public function getPurchaseOrderDetailColumn(Request $request) {
        try {
            /* 
            get file from specific path
            ie: /var/www/laravel/app/storage/column/stockholding/filename.json
            */
            $path = storage_path()."/column/purchaseOrderDetail/".strtolower($request->client).".json"; 
            $arr  = json_decode(file_get_contents($path), true);

            return response()->json(['data' => $arr], 200);
        } catch (Exception $ex) {
            return response()->json(["status" => 400, "message" => "Client Not Found!"], 400);
        }
    }

    public function putPurchaseOrderDetailColumn(Request $request) { 
        // validate request input
        $validator = Validator::make($request->all(), [
            'LINE_NO'        => 'required',
            'PRODUCT'        => 'required',
            'DESCRIPTION'    => 'required',
            'QTY'            => 'required',
            'UOM'            => 'required',
            'QTY_PROCESSED'  => 'required',
            'WEIGHT'         => 'required',
            'WEIGHT_PROCESSED' => 'required',
            'COMPLETED'      => 'required',
            'BATCH'          => 'required',
            'ROTA_DATE'      => 'required',
            'REF3'           => 'required',
            'REF4'           => 'required',
            'DISPOSITION'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => 400, "message" => $validator->errors()], 400);
        }

        if (!$request->input('client')) {
            return response()->json(["status" => 400, "message" => "Client Is Required!"], 400);
        }

        // mapping request input into required format
        $data = [
            [ 
            'LINE_NO'        => $request->LINE_NO,
            'PRODUCT'        => $request->PRODUCT,
            'DESCRIPTION'    => $request->DESCRIPTION,
            'QTY'            => $request->QTY,
            'UOM'            => $request->UOM,
            'QTY PROCESSED'  => $request->QTY_PROCESSED,
            'WEIGHT'         => $request->WEIGHT,
            'WEIGHT PROCESSED' => $request->WEIGHT_PROCESSED,
            'COMPLETED'      => $request->COMPLETED,
            'BATCH'          => $request->BATCH,
            'ROTA DATE'      => $request->ROTA_DATE,
            'REF3'           => $request->REF3,
            'REF4'           => $request->REF4 ,
            'DISPOSITION'    => $request->DISPOSITION
            ]
        ];
        
        /* 
        put file into specific path
        ie: /var/www/laravel/app/storage/column/stockholding/filename.json
        */
        Storage::put("/purchaseOrderDetail/".strtolower($request->input('client')).".json", json_encode($data));

        return response()->json(["status" => 200, "message" => "Config Saved!"], 200);
    }
}