<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');
    }

    public function getProduct(Request $request)
    {
        $client = $request->client;
        $param = $request->param;
        $productCode = [];
        $productName = [];
        if (!$client)
        {
            return response()->json('client required', 400);
        }
        try
        {
           $product = DB::table('is_product')
                        ->select('name', 'code')
                        ->where('client', $client)
                        ->where(function($query) use($param){
                            $query->where('code', 'like', '%'.$param.'%');
                        })
                        ->where('active_flag', 'Y')
                        ->orderBy('code')
                        ->take(1000)
                        ->get();
            return response()->json($product, 200);
        }
        catch(Exception $e)
        {
            return response()->json('failed to load data', 400);
        }
    }
}
