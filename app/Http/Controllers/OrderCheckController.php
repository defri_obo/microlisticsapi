<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderCheckController extends Controller
{
    public function __construct(){
        // $this->middleware('cors');
        // $this->middleware('loginAuth');
        // $this->middleware('jwt.verify');  
    }

    public function index(Request $request) {
        $input = $request->all();
        $rules = array(
            'order_no' => 'required',
            'client'   => 'required'
        );

        //validate request
        $validator = Validator::make($input, $rules);        
        if ($validator->fails()) {
            $arr = array('status' => 400, 'message' => $validator->errors()->first());
            return response()->json($arr);
        }
        
        //check existing order in IS_ORDER_HEAD table
        $check1 = 
            DB::table('IS_ORDER_HEAD')
            ->where('order_no', '=', $request->order_no)
            ->where('client', '=', strtoupper($request->client))
            ->first();
        
        //check existing order in IS_SO_SUPPDATA table
        $check2 = 
            DB::table('IS_SO_SUPPDATA')
            ->where('order_no', '=', $request->order_no)
            ->where('client', '=', strtoupper($request->client))
            ->first();

        //validate existing order
        if($check1 || $check2)
            return response()->json(['status' => 200, 'message' => 'not available']);
        
        return response()->json(['status' => 200, 'message' => 'available']);
    }
}
