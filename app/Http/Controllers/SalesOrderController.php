<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
class SalesOrderController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');
    }

    public function index(Request $request){
        try
        {
            $searchParam     = $request->searchParam;
            $client          = $request->client;
            $site            = $request->site;
            $status          = $request->status;
            $orderType       = $request->orderType;
            $task            = $request->task;

            $export          = $request->export; 
            if($export=='true'){
                $num = 75000;
                //remove param page
                $request->request->remove('page');
            }else{
                $num = 50;
            } 

            $result = DB::table('iw_so_head_v v')
                        ->select(DB::raw(  "site as site,
                                            site_name as site_name,
                                            client as client,
                                            client_name as client_name,
                                            order_no as orderNo,
                                            order_type as orderType,
                                            isis_task as isisTask,
                                            customer_name as customerName,
                                            status_desc as status,
                                            date_due as deliveryDate,
                                            date_recd as dateReceived,
                                            date_released as dateReleased,
                                            date_completed as dateCompleted,
                                            customer_po_no as customerPoNo,
                                            vendor_ord_no as vendorOrderNo,
                                            ship_to_address_1 as address1,
                                            ship_to_address_2 as address2,
                                            ship_to_address_3 as address3,
                                            ship_to_address_4 as address4,
                                            ship_to_address_5 as address5,
                                            ship_to_city as suburb,
                                            ship_to_postcode as postcode,
                                            ship_to_state as state,
                                            ship_to_country as country,
                                            load_number as loadNumber,
                                            loadout_start as loadoutStart,
                                            loadout_finish as loadoutFinish,
                                            consignment_number as consignmentNo,
                                            freight_charge as freightCharge,
                                            customer as customer
                                             "))
                        ->where(function($query) use($searchParam){
                            if($searchParam) $query->where('order_no', '=', $searchParam);
                        })
                        ->where(function($query) use($client){
                            if(isset($client) && $client !== 'all'){
                                $query->where('client', $client);
                            }
                        })
                        ->where(function($query) use($site){
                            if(isset($site) && $site !== 'all') {
                                $query->where('site', $site);
                            }
                        })
                        ->where(function($query) use($task){
                            if($task && $task !== 'All') $query->where('isis_task', $task);
                        })
                        ->where(function($query) use ($status){
                            if($status == 'open')
                            {
                                $query->where('STATUS_DESC', 'not like', '4%');
                            }
                            else if($status == 'released')
                            {
                                $query->where('STATUS_DESC', 'like', '2%');
                            }
                            else if($status == 'part_released')
                            {
                                $query->where('STATUS_DESC', 'like', '3%');
                            }
                            else if($status == 'available')
                            {
                                $query->where('STATUS_DESC', 'like', '1%');
                            }
                            else if($status == 'completed')
                            {
                                $query->where('STATUS_DESC', 'like', '4%');
                            }
                            else if($status == 'unavailable')
                            {
                                $query->where('STATUS_DESC', 'like', '0%');
                            }
                            else if($status == 'all')
                            {
                                return $query;
                            }
                        })
                        ->where(function($query) use($orderType){
                             if($orderType=="(NULL)" && isset($orderType)){
                                $query->where('order_type', NULL);
                            }elseif($orderType !== "all" && isset($orderType)){
                                $query->where('order_type', $orderType);
                            } 
                        })
                        ->orderByRaw('site,client,order_no')
                        ->paginate($num);  
            return response()->json(['data'=>$result],200);
        }
        catch(Exception $e)
        {
            response()->json(['message'=>'something wrong'],400);
        }
    }

    public function detail(Request $request, $orderNo){
        try
        {
            $client = $request->client;
            $orderNo = $request->orderNo; 

            $export          = $request->export; 
            if($export=='true'){
                $num = 75000;
                //remove param page
                $request->request->remove('page');
            }else{
                $num = 50;
            } 

            $result = DB::table('IW_SO_LINE_V v')
                        ->join('is_product t', function($join) use($client, $orderNo){
                            $join->on('v.product', 't.code')
                            ->on('v.client', 't.client');
                        })
                        ->selectRaw(' 
                            v.orig_line_number as LINE, 
                            v.product as PRODUCT,
                            t.name as PRODUCT_DESCRIPTION,
                            v.released as RELEASED, 
                            t.PACKDESC_1 as UOM,
                            v.qty_lcd as QTY, 
                            v.qty_processed as QTY_PROCESSED,
                            v.weight as WEIGHT, 
                            v.wgt_processed as WEIGHT_PROCESSED,
                            v.completed as COMPLETED, 
                            v.out_of_stock as OOS, 
                            v.ref as BATCH,
                            v.ref2 as REF2, 
                            v.ref3 as REF3, 
                            v.ref4 as REF4,
                            v.disposition as DISPOSITION, v.pack_id as PACK_ID')
                        ->where('v.order_no', $orderNo)
                        ->where('v.client', $client)
                    ->orderBy('v.orig_line_number','ASC')
                        ->paginate($num);
            return response()->json(['data'=>$result],200);
        }
        catch(Exception $e)
        {
            response()->json(['message'=>'something wrong'],400);
        }
    }

    public function getSoResourceCreation(Request $request)
    {
        try
        {
            $company = $request->company;
            $client  = $request->client;
            $userLevel  = $request->header('userLevel');

            if(!$client && !($userLevel == 'Admin' || $userLevel == 'Administrator'  ))
            { 
                return response()->json('client required',400);
            }

            $supplierName   = [];
            $supplierCode   = [];

            $siteName       = [];
            $siteCode       = [];

            $orderTypeName  = [];
            $orderTypeCode  = [];

            $orderTypeFilterName  = [];
            $orderTypeFilterCode  = [];

            $supplier = DB::table('is_customer_supplier_v ')
                    ->select('name', 'customer_no')
                    ->where(function ($query) use ($company, $client, $userLevel){
                        if($company && $company != '')
                        {
                            $query->where('customer_no', $company);
                        }
                        if($client && !$company || $company == '' && $userLevel !== 'Admin')
                        {
                            $query->where('client', $client);
                        }
                    })
                    ->get();


            $site = DB::table('is_site')
                    ->select('site','name')
                    ->get();

            $orderType = DB::table('is_code_description')
                        ->select('code', 'description')
                        ->where('type_key', 'SO_ORDER_TYPE')
                        ->get();

            $orderTypeFilter = DB::table('iw_so_head_v')
                                ->select('order_type')
                                ->distinct()
                                ->orderBy('order_type','asc')
                                ->get();


            // $uom = DB::table('is_product')
            //             ->select( 'PACKDESC_1', 'PACKDESC_4', 'PACKDESC_5')
            //             ->where('client', $client)
            //             ->where('code', $productCode)
            //             ->get();

            if(count($supplier) > 0)
            {
                foreach($supplier as $sup)
                {
                    array_push($supplierName, $sup->name);
                    array_push($supplierCode, $sup->customer_no);
                }
            }

            if(count($site) > 0)
            {
                foreach($site as $s)
                {
                    $sites = $s->site.':'.$s->name;
                    array_push($siteName, $sites);
                    array_push($siteCode, $s->site);
                }
            }

            if(count($orderType) > 0)
            {
                foreach($orderType as $ord)
                {
                    array_push($orderTypeName, $ord->description);
                    array_push($orderTypeCode, $ord->code);
                }
            }

            if(count($orderTypeFilter) > 0)
            {
                foreach($orderTypeFilter as $ord)
                {
                     if($ord->order_type==null){
                        array_push($orderTypeFilterName, "(NULL)");
                        array_push($orderTypeFilterCode, "(NULL)");
                    }else{
                        array_push($orderTypeFilterName, $ord->order_type);
                        array_push($orderTypeFilterCode, $ord->order_type);
                    }
                }
            }

            $supplier   = [ 'code' => $supplierCode,
                            'name' => $supplierName
                        ];

            $site       = [ 'code' => $siteCode,
                            'name' => $siteName
                        ];

            $orderType  = [ 'code' => $orderTypeCode,
                            'name' => $orderTypeName
                        ];

            $orderTypeFilter  = [   'code' => $orderTypeFilterCode,
                                    'name' => $orderTypeFilterName
                                ];

            if($company && $company != 'undefined' && $company != 'null')
            {
                $identity = DB::table('IS_CUSTOMER_SUPPLIER_V')
                        ->select('name', 'customer_no','address_1','address_2','address_3','address_4','address_5','city','postcode','state','country')
                        ->where('client', $client)
                        ->where('customer_no', $supplier['code'] )
                        ->get();

                return response()->json([ 'supplier' => $supplier, 'site' => $site, 'orderType' => $orderType, 'identity' => $identity ],200);
            }

            return response()->json([ 'supplier' => $supplier, 'site' => $site, 'orderType' => $orderType, 'orderTypeFilter' => $orderTypeFilter],200);


        }
        catch(Exception $e)
        {
            return response()->json(['message' => 'failed to load'],400);
        }
    }

    public function getSoIdentity(Request $request){
       try
       {

        $client  = $request->client;
        $customerNo = $request->customerNo;
        $identity = DB::table('IS_CUSTOMER_SUPPLIER_V')
                    ->select('name','customer_no','address_1','address_2','address_3','address_4','address_5','city','postcode','state','country')
                    ->where('client', $client)
                    ->where('customer_no', $customerNo )
                    ->get();
        return response()->json(['identity' => $identity ],200);
       }
       catch(Exception $e)
       {
         return response()->json(['message' => 'failed to load'],400);
       }
    }

    public function store(Request $request)
    {
        $sequence                       = DB::getSequence();
        $datas                          = $request->all();
        $datas                          = (array) $datas;
        $orderDetails                   = $datas['header'];
        $orderLines                     = $datas['lineDetail'];
        $seq_IS_IFAC                    = $sequence->nextValue('IS_IFAC_SEQ');
        $seq_IS_IFAC                    = (int)$seq_IS_IFAC;
        $currentDate                    = Carbon::now();
        $IS_IFAC_HEADER = [
            'ifac_key'                  => $seq_IS_IFAC,
            'client'                    => $orderDetails['client'],
            'stream_id'                 => 'OR',
            'packet_number'             => $seq_IS_IFAC,
            'record_number'             => 1,
            'table_level'               => 0,
            'transmit_indicator'        => 'SN',
            'rejection_reason'          => null,
            'rejection_status'          => null,
            'process_status'            => 'P',
            'transfer_date'             => $currentDate
        ];

        $IS_IFAC_OR_1 = [
            'action_indicator'          => 'A',
            'ifac_key'                  => $seq_IS_IFAC,
            'customer_number'           => $orderDetails['customerVal'],
            'order_type'                => $orderDetails['orderType'],
            'order_date'                => $orderDetails['deliveryDate'],
            'isis_task'                 => null,
            'cust_order_number'         => $orderDetails['orderId'],
            'customer_po_number'        => $orderDetails['customerOrderRef'],
            'vendor_ord_no'             => $orderDetails['vendorOrderRef'],
            'e_t_d'                     => $orderDetails['deliveryDate'],
            'ship_to_name'              => $orderDetails['customer']['value'],
            'ship_to_address_1_1'       => $orderDetails['shipToAddress1'],
            'ship_to_address_1_2'       => $orderDetails['shipToAddress2'],
            'ship_to_address_1_3'       => $orderDetails['shipToAddress3'],
            'ship_to_address_1_4'       => $orderDetails['shipToAddress4'],
            'ship_to_address_1_5'       => $orderDetails['shipToAddress5'],
            'city'                      => $orderDetails['city'],
            'postcode'                  => $orderDetails['postCode'],
            'state'                     => $orderDetails['state'],
            'country'                   => $orderDetails['country'],
            'delivery_desc_1'           => $orderDetails['deliveryInstruction'],
            'delivery_desc_2'           => $orderDetails['deliveryInstruction'],
            'delivery_desc_3'           => $orderDetails['deliveryInstruction'],
            'delivery_desc_4'           => $orderDetails['deliveryInstruction'],
            'site'                      => $orderDetails['site']
        ];

        $IS_IFAC_2 = [];

        $IS_IFAC_OR_2 = [];
        $sq = [];
        foreach($orderLines as $index => $lineDetail){

            $number     = $index+1;
            $handling   = null;
            $qty        = null;
            $weight     = null;
            $handling = DB::table('IS_PRODUCT ')
                        ->select('variable_weight')
                        ->where('client', $orderDetails['client'])
                        ->where('active_flag', 'Y')
                        ->where('code', $lineDetail['productVal'])
                        ->first();
            if($handling)
            {
                if($handling->variable_weight > 0)
                {
                    $handling = 'W';
                    $qty        = 1;
                    $weight     = $lineDetail['weight'];
                }
                else
                {
                    $handling   = 'Q';
                    $qty        = $lineDetail['qty'];
                    $weight     = 0;
                }
            }
            $seq_IS_IFAC2                    = $sequence->nextValue('IS_IFAC_SEQ');
            $IS_IFAC_2_ARRAY = [
                'ifac_key'              => $seq_IS_IFAC2,
                'client'                => $orderDetails['client'],
                'stream_id'             => 'OR',
                'packet_number'         => $seq_IS_IFAC,
                'record_number'         => 1,
                'table_level'           => 1,
                'transmit_indicator'    => 'SN',
                'rejection_reason'      => null,
                'rejection_status'      => null,
                'process_status'        => 'P',
                'transfer_date'         => $currentDate
            ];

            $IS_IFAC_OR_2_ARRAY = [
                'action_indicator'      => 'A',
                'ifac_key'              => $seq_IS_IFAC2,
                'line_number'           => $number,
                'product'               => $lineDetail['productVal'],
                'quantity_ordered'      => $qty,
                'uom'                   => $lineDetail['uom'],
                'ref'                   => $lineDetail['batch'],
                'ref3'                  => $lineDetail['ref3'],
                'ref4'                  => $lineDetail['ref4'],
                'disposition'           => $lineDetail['dispositionVal'],
                'pack_id'               => $lineDetail['packId'],
                'handling'              => $handling,
                'weight'                => $lineDetail['weight'],
                'rotadate'              => $lineDetail['rotaDate'],
                'weight'                => $weight,
                'cust_order_number'     => $orderDetails['orderId']
            ];
            array_push($IS_IFAC_2, $IS_IFAC_2_ARRAY);
            array_push($IS_IFAC_OR_2, $IS_IFAC_OR_2_ARRAY);
        }

        try
        {
            DB::beginTransaction();
            DB::table('is_ifac')->insert($IS_IFAC_HEADER);
            DB::table('is_ifac_or1')->insert($IS_IFAC_OR_1);
            DB::table('is_ifac')->insert($IS_IFAC_2);
            DB::table('is_ifac_or2')->insert($IS_IFAC_OR_2);
            DB::commit();
            return response()->json('create successfully', 200);
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return response()->json('create failed', 400);
        }
    }
}
