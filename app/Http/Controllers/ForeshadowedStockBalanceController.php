<?php

namespace App\Http\Controllers;

use App\Models\StockholdingModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ForeshadowedStockBalanceController extends Controller
{
    public function __construct(){
        // $this->middleware('cors');
        // $this->middleware('loginAuth');
        // $this->middleware('jwt.verify');  
    }
    
    public function index() {  
        $foreshadowedStockBalances = DB::table('IW_STOCK_HOLDING_V')
        ->join('IW_STOCK_MOVEMENT_V', 'IW_STOCK_HOLDING_V.product', '=', 'IW_STOCK_MOVEMENT_V.product')
        ->get();

        return response()->json(['data' => $foreshadowedStockBalances]);
    }

    public function show($id) {
        DB::enableQueryLog();
        $foreshadowedStockBalance = DB::table('IW_STOCK_HOLDING_V')
        ->join('IW_STOCK_MOVEMENT_V', 'IW_STOCK_HOLDING_V.product', '=', 'IW_STOCK_MOVEMENT_V.product')
        ->where('IW_STOCK_HOLDING_V.product', '=', $id)
        ->get();
        // dd(DB::getQueryLog());

        return response()->json(['data' => $foreshadowedStockBalance]);
    }

    public function stockbal(Request $request){        
        $validator = Validator::make($request->all(), [
            'client'       => 'required',
            'product'      => 'required',
            'site'         => 'required'
        ]);

        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first());
            return response()->json($arr);
        }

        $step1 =         
        DB::table(DB::raw('IW_STOCK_HOLDING_V s, IS_PRODUCT p'))
        ->select(DB::raw("
            batch as batchNum,
            NVL (s.STATUS,'-') as stockStatus,
            sum (qty_lcd) as quantity,
            case
                when upper (p.rotadate_type)= 'U'
                then round (s.effective_date-sysdate) 
                else round (s.effective_date+p.expiry_days-sysdate)
                end as stockExpirydays,
            case 
                when upper (p.rotadate_type) = 'U'
                then to_char (s.effective_date, 'dd/MM/YYYY')
                else to_char (s.effective_date+p.expiry_days, 'dd/MM/YYYY')
                end as stockExpiryDate,
            sum(s.weight) as weight

        "))
        ->where([            
            's.client'  => $request->client,
            's.product' => $request->product,
            's.site'    => $request->site,
            'type'      => 'H'

        ])        
        ->whereRaw('s.client = p.client')
        ->whereRaw('s.product = p.code')

        ->groupBy('s.batch', 's.status', 's.effective_date', 'p.rotadate_type', 'p.expiry_days')
        ->orderByRaw('effective_date NULLS FIRST') 
        ->get();
        // dd($step1);
        // return response()->json([$step1]);

        $openingBalance = $step1->sum('quantity');
        $lastStockDate  = $step1->isEmpty() ? null : ($step1->last())->stockexpirydate;
        // dd($openingBalance, $lastStockDate);

        $step2 = 
        DB::table('IW_STOCK_HOLDING_V')
        ->select(DB::raw("
            order_no as orderNo,
            company as company,
            TO_CHAR(NVL(effective_date,sysdate),'dd/MM/YYYY') as effectiveDate,
            ROUND (NVL(effective_date,sysdate) - sysdate) as daysFromCR,
            qty_lcd as quantity,
            QTY_LCD_EXPECTED as qtyExpected,
            QTY_LCD_COMMITTED as qtyCommitted,
            type as type,
            NVL(STATUS, '-') as stockStatus,
            weight as weight
        "))
        ->whereRaw("
            client      = '$request->client'
            AND product = '$request->product' 
            AND site    = '$request->site'
            AND type IN ('S','P')
        ")
        ->orderByRaw("effective_date NULLS FIRST, DECODE (type, 'P', 1, 'S', 2, 'H', 3)")
        // ->take(20)
        ->get();

        $a = collect(json_decode(json_encode($step1), true))->sortBy(function ($value) { return strtotime(str_replace('/', '-', $value['stockexpirydate'])); })->groupBy(['batchnum'])->toArray();
        $b = collect(json_decode(json_encode($step2), true))->groupBy(['stockstatus'])->toArray();

        foreach ($b as $c) foreach ($c as $d)
            if (array_key_exists($d['stockstatus'], $a)) {
                if ($d['type'] === 'P') {
                    $a[$d['stockstatus']][count($a[$d['stockstatus']]) - 1]['quantity'] += $d['qtyexpected'];
                } elseif ($d['type'] === 'S') {
                    $e = $d['qtycommitted'];

                    foreach ($a[$d['stockstatus']] as $f => $g) {
                        $a[$d['stockstatus']][$f]['quantity'] = $g['quantity'] - $e >= 0 ? $g['quantity'] - $e : 0;
                        $a[$d['stockstatus']][$f]['weight']   = $a[$d['stockstatus']][$f]['quantity'];
                        $e                                    = $e - $g['quantity'] >= 0 ? $e - $g['quantity'] : 0;
                    }
                }
            } else {
                if ($d['type'] === 'P') $a[$d['stockstatus']][] = ['batchnum' => $d['stockstatus'], 'stockstatus' => $d['stockstatus'], 'quantity' => $d['qtyexpected'], 'stockexpirydays' => date_diff(date_create(date('Y-m-d', strtotime(str_replace('/', '-', $d['effectivedate'])))), date_create(date('Y-m-d')))->format('%r%a') - 1, 'stockexpirydate' => $d['effectivedate'], 'weight' => $d['qtyexpected']];
            }

        $a = collect($a)->flatten(1)->sortBy(function ($value, $key) { return strtotime(str_replace('/', '-', $value['stockexpirydate'])).'-'.$value['batchnum']; })->toArray();
        $f = array_values($a);
        // dd($f);

        // return response()->json([$f]);

        if($step2->isEmpty()){
            $lastOpeningBalance[] = [
                'opening balance'      => null,
                'opening balance date' => null
            ];
    
            $lastClosingBalance[] = [
                'closing balance'      => null,
                'closing balance date' => null
            ];
        } else {
            $i = 0;
            foreach ($step2 as $key => $value) {
                $a = $i++;
                $availableOrders[] = [
                    'openingbalance' => $openingBalance,
                    'qtycommitted'   => $value->qtycommitted,
                    'closingbalance' => $openingBalance-$value->qtycommitted,
                    'qtyexpected'    => $value->qtyexpected,
                    'effectivedate'  => $value->effectivedate,
                    'stockstatus'    => $value->stockstatus,
                    'quantity'       => $value->quantity,
                    'orderno'        => $value->orderno,
                    'company'        => $value->company,
                    'daysfromcr'     => $value->daysfromcr,
                    'type'           => $value->type,
                ];

                if($availableOrders[$a]['type'] === "S"){
                    $closingbalance[] = [
                        'closingbalance' => $openingBalance - $value->qtycommitted
                    ];
                }else{
                    $closingbalance[] = [
                        'closingbalance' => $openingBalance + $value->qtyexpected
                    ];
                }

                $availableOrders = array_replace_recursive($availableOrders, $closingbalance);
                $openingBalance  = $availableOrders[$a]['closingbalance'];
            }

            $lastOpeningBalance[] = [
                'opening balance'      => current($availableOrders)['openingbalance'],
                'opening balance date' => $lastStockDate
            ];

            $lastClosingBalance[] = [
                'closing balance'      => end($availableOrders)['closingbalance'],
                'closing balance date' => $lastStockDate
            ];
        }

        $return[] = [
            'opening balance'  => $lastOpeningBalance[0]['opening balance'],
            'closing balance'  => $lastClosingBalance[0]['closing balance'],
            'available orders' => $availableOrders,
            // 'stock expiry'     => $step1->toArray()
            'stock expiry'     => $f
        ];

        // dd($return);

        // dd($availableOrders, $lastOpeningBalance, $lastClosingBalance);

        return response()->json([$return]);
    }
}