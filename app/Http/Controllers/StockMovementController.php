<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class StockMovementController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');                
        $this->middleware('cors');
    }

    public function loadStockMovement(Request $request)
    {
        $startDate  = $request->startDate;
        $endDate    = $request->endDate;
        $filterType = $request->filterType;

        $site    = $request->site;
        $client  = $request->client;
        $product = $request->product;
        $array   = [];
        
        //Date format validation
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$startDate))
        {
            return response()->json(['error message'=>'date format must be (yyyy-mm-dd)'],400);
        }

        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$endDate))
        {
            return response()->json(['error message'=>'date format must be (yyyy-mm-dd)'],400);
        }

        //Filter by day
        if($filterType == 'day')
        {
            while(date($startDate) <= date($endDate))
            {                    
                $result = $this->getStockAgeMovementData($startDate, $endDate, $filterType,  $site, $client, $product);
                if(count($result) > 0)
                {
                    array_push($array,$result);                    
                }
                $startDate = date('Y-m-d', strtotime("+1 day", strtotime($startDate)));
            }  
        }

        //Filter by week
        if($filterType == 'week')
        {
            $date2 = date('Y-m-d', strtotime("+7 day", strtotime($startDate))); 
            while(date($startDate) <= date($endDate))
            {
                
                $date2 = date('Y-m-d', strtotime("+7 day", strtotime($startDate)));
                if($date2 > $endDate)
                {
                    $date2 = $endDate;
                }
                $result = $this->getStockAgeMovementData($startDate, $date2, $filterType, $site, $client, $product);
                if($result !== [])
                {
                    array_push($array,$result);
                }
                $startDate = date('Y-m-d', strtotime("+7 day", strtotime($startDate)));
                
            }
        }
    
        //Filter by month
        if($filterType == 'month')
        {
            while(date($startDate) <= date($endDate))
            {                    
                $result = $this->getStockAgeMovementData($startDate, $endDate, $filterType, $site, $client, $product);
                if($result !== [])
                {
                    array_push($array,$result);
                }
                $startDate = date('Y-m-d', strtotime("+1 month", strtotime($startDate)));                    
            }   
        }

        //Load all Stock Movement list
        if($filterType == null)
        {
            $array = $this->getStockAgeMovementData($startDate, $endDate, $filterType, $site, $client, $product);
        }

        if(count($array) > 0)
        {
            $newArray = [];
            $site     = [];
            $client   = [];
            $product  = [];
            foreach($array as $data )
            {
                foreach($data as $key => $object)
                {
                    array_push($newArray, $object); 
                }
            }

            //start sorting multidimensional array object
            foreach($newArray as $key => $object)
            {
                $client[$key]   = $object['client'];
                $product[$key]  = $object['product'];
                $site[$key]     = $object['site'];
            }

            $client  = array_column($newArray, 'client');
            $product = array_column($newArray, 'product');
            $site    = array_column($newArray, 'site');
            array_multisort($site,SORT_ASC ,$client, SORT_ASC, $product, SORT_ASC, $newArray);
            //end sorting multidimensional array object


                // $ee = $this->grouping($newArray);
                // dd($ee);
            $group = collect($newArray)->groupBy([function ($item, $key) { return $item['product'].'|'.$item['packdesc']; }])->toArray();
            // dd($group);
            foreach ($group as $key => $value) {
                $return[$key] = [
                    'product'      => $value[0]['product'],
                    'packdesc'     => $value[0]['packdesc'],
                    'site'         => $value[0]['site'],
                    'product_name' => $value[0]['product_name'],
                    'client' => $value[0]['client'],
                ];

                foreach ($value as $product => $value1)
                    $return[$key]['detail'][] = [
                        'date'        => $value1['date'],
                        'sa_plus'     => $value1['sa_plus'],
                        'sa_minus'    => $value1['sa_minus'],
                        'recv_weight' => $value1['recv_weight'],
                        'send_weight' => $value1['send_weight']
                    ];
            }
            // dd($return);
            $datas = array_values($return); 
            return response()->json(['data'=>$datas], 200);
        }
        else
        {
            return response()->json(['data'=>$array], 200);
        }
    }

    function grouping($input) {
        $output = array();
      
        foreach($input as $value) {
            $output_element = &$output[$value['product'] . "_" . $value['packdesc']];
            $output_element['product']      = $value['product'];
            $output_element['packdesc']     = $value['packdesc'];
            $output_element['product_name'] = $value['product_name'];
            $output_element['site']         = $value['site'];
        }
      
        return array_values($output);
    }

    public function getStockAgeMovementData($startDate, $endDate, $filterType, $site="", $client="", $product="")
    {
        try
        {
            $result = DB::table('iw_stock_movement_v')
                    ->select('iw_stock_movement_v.product', 'iw_stock_movement_v.site', 'iw_stock_movement_v.product_name','is_product.packdesc_1','iw_stock_movement_v.client',
                            DB::raw('sum(iw_stock_movement_v.sa_plus) as sa_plus'),DB::raw('sum(iw_stock_movement_v.sa_minus) as sa_minus'),
                            DB::raw('sum(iw_stock_movement_v.qty_rec) as recv_weight'),DB::raw('sum( iw_stock_movement_v.qty_send) as send_weight'))
                    ->join('is_product', 'iw_stock_movement_v.product', '=', 'is_product.code')
                    ->where(function($query) use($startDate,$endDate, $filterType){                        
                    if($filterType == 'day'){
                        $query->whereRAW("TRUNC(SM_DTM) = TO_DATE('$startDate', 'yyyy-mm-dd')");                        }
                    })  
                    ->where(function($query) use($startDate,$endDate, $filterType){                        
                        if($filterType == 'week'){
                            $query->whereRAW("SM_DTM between to_date('$startDate', 'yyyy-mm-dd') and to_date('$endDate', 'yyyy-mm-dd')");
                        }
                    })
                    ->where(function($query) use($startDate,$endDate, $filterType){                        
                        if($filterType == 'month'){
                            $month = date_format(date_create($startDate), 'm');                            
                            $year = date_format(date_create($startDate), 'Y');
                            $query->whereRAW("to_char(SM_DTM,'MM')='$month' AND to_char(SM_DTM,'YYYY')='$year'");
                        }
                    })
                    ->where(function($query) use($site){   
                        if(isset($site) && $site !== 'all'){
                            $query->where("iw_stock_movement_v.site",$site); 
                        }   
                    })
                    ->where(function($query) use($client){   
                        if(isset($client) && $client !== 'all'){
                            $query->where("iw_stock_movement_v.client",$client); 
                        }   
                    })
                    ->where(function($query) use($product){   
                        if(isset($product) && $product !== 'all'){
                            $query->where("iw_stock_movement_v.product",$product); 
                        }   
                    })
                    ->groupBy('iw_stock_movement_v.product', 'iw_stock_movement_v.site', 'iw_stock_movement_v.product_name','is_product.packdesc_1','iw_stock_movement_v.client')
                    ->orderByRaw('iw_stock_movement_v.site,iw_stock_movement_v.client,iw_stock_movement_v.product ASC')
                    ->get(); 
                    $data =[];


                    foreach($result as $res => $value)
                    {
                        $modified =[   
                                'product'      => $value->product,
                                'site'         => $value->site,
                                'product_name' => $value->product_name,
                                'packdesc'     => $value->packdesc_1,
                                'date'         => $startDate,
                                'sa_plus'      => $value->sa_plus,
                                'sa_minus'     => $value->sa_minus,
                                'recv_weight'  => $value->recv_weight,
                                'send_weight'  => $value->send_weight,
                                'client'       => $value->client
                            ];
                        array_push($data, $modified);
                    }
            return $data;
        }
        catch(Exception $e)
        {
            return $e;
        }        
    }

    public function stockDateRange(Request $request)
    {  
        try
        {
            $result = DB::table('iw_stock_movement_v')
                    ->select( 
                        DB::raw("TO_CHAR(max(SM_DTM),'yyyy-mm-dd') as max_date"), 
                        DB::raw("TO_CHAR(min(SM_DTM),'yyyy-mm-dd') as min_date")) 
                    ->get();     

            return response()->json(['data'=>$result], 200);
        }
        catch(Exception $e)
        {
            
            return response()->json(['Err'=>$e], 500);
        }         
    }

}