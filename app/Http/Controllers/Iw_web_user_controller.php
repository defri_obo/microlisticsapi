<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Iw_web_user_controller extends Controller
{
    public function __construct(){
        // $this->middleware('cors');
        // $this->middleware('loginAuth');
        // $this->middleware('jwt.verify');
    }

    public function index(Request $request) {
        $WEB_USER        = $request->web_user;
        $PASSWORD        = $request->password;
        $CLIENT          = $request->client;
        $WEB_GROUP       = $request->web_group;
        $COMPANY         = $request->company;
        $SESSION_ID      = $request->session_id;
        $EMAIL           = $request->email;
        $PASSWORD_CHANGE = $request->password_change;
        $LAST_ACCESS     = $request->last_access;
        $LAST_LOGIN      = $request->last_login;
        $LAST_IP         = $request->last_ip;
        $THIS_ACCESS     = $request->this_access;
        $THIS_LOGIN      = $request->this_login;
        $THIS_IP         = $request->this_ip;
        $DISABLED        = $request->disabled;
        $DISPLAY_UNITS   = $request->display_units;
        $USERID          = $request->userid;
        $CARRIER         = $request->carrier;
        $NAME            = $request->name;
        $searchParam     = $request->searchParam;

        $export          = $request->export; 
        if($export=='true'){
            $num = 75000;
            //remove param page
            $request->request->remove('page');
        }else{
            $num = 50;
        }
         
        try
        {
            $users = DB::table('IW_WEB_USER')
            ->select(DB::raw("
                                WEB_USER,
                                PASSWORD,
                                CLIENT,
                                WEB_GROUP,
                                COMPANY,
                                SESSION_ID,
                                EMAIL,
                                PASSWORD_CHANGE,
                                LAST_ACCESS,
                                LAST_LOGIN,
                                LAST_IP,
                                THIS_ACCESS,
                                THIS_LOGIN,
                                THIS_IP,
                                DISABLED,
                                DISPLAY_UNITS,
                                USERID,
                                CARRIER,
                                NAME,
                                SITE
                            "))
            ->where(function($query) use ($searchParam){
                if($searchParam){
                    $query->whereRaw("lower(NAME) LIKE lower('%$searchParam%')")
                    ->orWhere('USERID', 'like', '%' . $searchParam . '%');
                }
            })
            ->where(function($query) use ($WEB_USER){
                if($WEB_USER)
                {
                    $query->where('WEB_USER', 'like', '%' . $WEB_USER . '%');
                }
            })
            ->where(function($query) use ($PASSWORD){
                if($PASSWORD)
                {
                    $query->where('PASSWORD', 'like', '%' . $PASSWORD . '%');
                }
            })
            ->where(function($query) use ($CLIENT){
                if($CLIENT)
                {
                    $query->where('CLIENT', 'like', '%' . $CLIENT . '%');
                }
            })
            ->where(function($query) use ($WEB_GROUP){
                if($WEB_GROUP)
                {
                    $query->where('WEB_GROUP', 'like', '%' . $WEB_GROUP . '%');
                }
            })
            ->where(function($query) use ($COMPANY){
                if($COMPANY)
                {
                    $query->where('COMPANY', 'like', '%' . $COMPANY . '%');
                }
            })
            ->where(function($query) use ($SESSION_ID){
                if($SESSION_ID)
                {
                    $query->where('SESSION_ID', 'like', '%' . $SESSION_ID . '%');
                }
            })
            ->where(function($query) use ($EMAIL){
                if($EMAIL)
                {
                    $query->where('EMAIL', 'like', '%' . $EMAIL . '%');
                }
            })
            ->where(function($query) use ($PASSWORD_CHANGE){
                if($PASSWORD_CHANGE)
                {
                    $query->where('PASSWORD_CHANGE', 'like', '%' . $PASSWORD_CHANGE . '%');
                }
            })
            ->where(function($query) use ($LAST_ACCESS){
                if($LAST_ACCESS)
                {
                    $query->where('LAST_ACCESS', 'like', '%' . $LAST_ACCESS . '%');
                }
            })
            ->where(function($query) use ($LAST_LOGIN){
                if($LAST_LOGIN)
                {
                    $query->where('LAST_LOGIN', 'like', '%' . $LAST_LOGIN . '%');
                }
            })
            ->where(function($query) use ($LAST_IP){
                if($LAST_IP)
                {
                    $query->where('LAST_IP', 'like', '%' . $LAST_IP . '%');
                }
            })
            ->where(function($query) use ($THIS_ACCESS){
                if($THIS_ACCESS)
                {
                    $query->where('THIS_ACCESS', 'like', '%' . $THIS_ACCESS . '%');
                }
            })
            ->where(function($query) use ($THIS_LOGIN){
                if($THIS_LOGIN)
                {
                    $query->where('THIS_LOGIN', 'like', '%' . $THIS_LOGIN . '%');
                }
            })
            ->where(function($query) use ($THIS_IP){
                if($THIS_IP)
                {
                    $query->where('THIS_IP', 'like', '%' . $THIS_IP . '%');
                }
            })
            ->where(function($query) use ($DISABLED){
                if($DISABLED)
                {
                    $query->where('DISABLED', 'like', '%' . $DISABLED . '%');
                }
            })
            ->where(function($query) use ($DISPLAY_UNITS){
                if($DISPLAY_UNITS)
                {
                    $query->where('DISPLAY_UNITS', 'like', '%' . $DISPLAY_UNITS . '%');
                }
            })
            ->where(function($query) use ($USERID){
                if($USERID)
                {
                    $query->where('USERID', 'like', '%' . $USERID . '%');
                }
            })
            ->where(function($query) use ($CARRIER){
                if($CARRIER)
                {
                    $query->where('CARRIER', 'like', '%' . $CARRIER . '%');
                }
            })
            ->where(function($query) use ($NAME){
                if($NAME)
                {
                    $query->where('NAME', 'like', '%' . $NAME . '%');
                }
            })
            ->orderBy('NAME', 'asc')
            ->paginate($num);

            return response()->json(['data' => $users], 200);
        }

        catch(Exception $e)
        {
            return response()->json(['message'=> $e->getMessage()], 400);
        }
    }

    public function show($web_user) {
        // DB::enableQueryLog();
        $user_detail = DB::table('IW_WEB_USER')
        ->select(
            'IW_WEB_USER.CARRIER' ,
            'IW_WEB_USER.CLIENT' ,
            'IW_WEB_USER.COMPANY' ,
            'IW_WEB_USER.DISABLED' ,
            'IW_WEB_USER.DISPLAY_UNITS' ,
            'IW_WEB_USER.EMAIL' ,
            'IW_WEB_USER.LAST_ACCESS' ,
            'IW_WEB_USER.LAST_IP' ,
            'IW_WEB_USER.LAST_LOGIN' ,
            'IW_WEB_USER.NAME' ,
            'IW_WEB_USER.SESSION_ID' ,
            'IW_WEB_USER.THIS_ACCESS' ,
            'IW_WEB_USER.THIS_IP' ,
            'IW_WEB_USER.THIS_LOGIN' ,
            'IW_WEB_USER.USERID' ,
            'IW_WEB_USER.WEB_GROUP' ,
            'IW_WEB_USER.WEB_USER' ,
            'IW_WEB_USER.SITE' ,
            'IW_USER_MENU.MENU_ID' ,
            'IW_MENU_ITEMS.MENU_NAME'
            )
        ->join('IW_USER_MENU', 'IW_USER_MENU.WEB_USER', '=', 'IW_WEB_USER.WEB_USER')
        ->join('IW_MENU_ITEMS', 'IW_USER_MENU.MENU_ID', '=', 'IW_MENU_ITEMS.MENU_ID')
        ->where('IW_WEB_USER.WEB_USER', '=', $web_user)
        ->get();

        $data =[];
        foreach($user_detail as $user => $value){
            $modified =[
                    'carrier'       => $value->carrier,
                    'userid'        => $value->userid,
                    'name'          => $value->name,
                    'web_user'      => $value->web_user,
                    'email'         => $value->email,
                    'client'        => $value->client,
                    'menu_id'       => $value->menu_id,
                    'company'       => $value->company,
                    'disabled'      => $value->disabled,
                    'display_units' => $value->display_units,
                    'last_access'   => $value->last_access,
                    'last_ip'       => $value->last_ip,
                    'last_login'    => $value->last_login,
                    'session_id'    => $value->session_id,
                    'this_access'   => $value->this_access,
                    'this_ip'       => $value->this_ip,
                    'this_login'    => $value->this_login,
                    'web_group'     => $value->web_group,
                    'menu_name'     => $value->menu_name,
                    'site'          => $value->site
                ];
            array_push($data, $modified);
        }

        $group = collect($data)->groupBy([function ($item, $key) { return $item['userid'].'|'.$item['web_user']; }])->toArray();

        foreach ($group as $key => $value) {
            $return[$key] = [
                'carrier'       => $value[0]['carrier'],
                'userid'        => $value[0]['userid'],
                'name'          => $value[0]['name'],
                'web_user'      => $value[0]['web_user'],
                'email'         => $value[0]['email'],
                'client'        => $value[0]['client'],
                'menu_id'       => $value[0]['menu_id'],
                'company'       => $value[0]['company'],
                'disabled'      => $value[0]['disabled'],
                'display_units' => $value[0]['display_units'],
                'last_access'   => $value[0]['last_access'],
                'last_ip'       => $value[0]['last_ip'],
                'last_login'    => $value[0]['last_login'],
                'session_id'    => $value[0]['session_id'],
                'this_access'   => $value[0]['this_access'],
                'this_ip'       => $value[0]['this_ip'],
                'this_login'    => $value[0]['this_login'],
                'web_group'     => $value[0]['web_group'],
                'site'          => $value[0]['site']
            ];

            foreach ($value as $web_user => $value1)
                $return[$key]['module'][] = [
                    'menu_id'   => $value1['menu_id'],
                    'menu_name' => $value1['menu_name']
                ];

            $datas = array_values($return);
            return response()->json(['data'=>$datas], 200);
        }
    }
}
