<?php

namespace App\Http\Controllers;

use App\Models\IS_STOCK_V_Model;
use App\Models\IW_STOCKHOLDING_V_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class StockholdingController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');
    }

    public function test(){
        $data = DB::table('v_stock_holding_summary')
                ->get();
        return $data;
    }

    public function index(Request $request) {
        $searchParam = $request->searchParam;
        $site        = $request->site;
        $status      = $request->status;
        $UOM         = $request->UOM;
        $clientCode  = $request->client;
        $web_user    = $request->header('webUser');
        $clientHeader    = $request->header('client');
        $task            = $request->task;
        
        $export          = $request->export; 
        if($export=='true'){
            $num = 75000;
            //remove param page
            $request->request->remove('page');
        }else{
            $num = 50;
        } 

        try{
            DB::enableQueryLog();
            $stockholding =
                DB::table(DB::raw('iw_stock_holding_v v, is_product p'))
                ->select(DB::raw("
                        v.product,
                        v.client,
                        v.site,
                        max(v.product_name) as product_name,
                        max(v.status) as Disposition,
                        sum(v.weight) as weight,
                        sum(v.qty_lcd) as On_Hand_Qty,
                        sum(v.qty_lcd_expected) as expected_in_qty,
                        sum(v.qty_lcd_committed) AS expected_out_qty,
                        sum(v.wgt_expected) as expected_in_wgt,
                        max(p.packdesc_1) as packdesc_1,
                        max(v.effective_date) as Rotadate,
                        sum(v.qty_price) as Price,
                        sum(v.qty_hcd) as Pallets
                        "))
                ->where('v.product','=', DB::raw('p.code'))
                ->where('v.client','=', DB::raw('p.client'))
                ->where(function($query) use ($clientHeader,$clientCode){
                    if($clientHeader){
                        $query->where('v.client', '=', $clientHeader);
                    }elseif(isset($clientCode)  && $clientCode !== 'all'){
                        $query->where('v.client', '=', $clientCode);
                    }
                })
                ->where(function($query) use ($searchParam){
                    if($searchParam){
                        $query->where('v.product_name', 'like', '%' . $searchParam . '%')
                        ->orWhere('v.product', 'like', '%' . $searchParam . '%');
                    }
                })
                ->where(function($query) use ($site){
                    if(isset($site) && $site !== 'all'){
                        $query->where('v.site', '=', $site);
                    }
                })
                ->where(function($query) use ($status){
                    if($status=="Ok"){
                        $query->where('v.qty_lcd_committed','<=', DB::raw('(v.QTY_LCD + v.QTY_LCD_EXPECTED)'));
                    }elseif($status=="Shortage"){
                        $query->where('v.qty_lcd_committed','>', DB::raw('(v.QTY_LCD + v.QTY_LCD_EXPECTED)'));
                    }
                })
                ->orderBy(DB::raw('v.site, v.client, v.product'))
                ->groupBy(DB::raw('v.site, v.client, v.product'))
                ->paginate($num);

            return response()->json(['data' => $stockholding], 200);
        }
        catch(Exception $e){
            return response()->json(['error' => $e], 404);
        }
    }
}
