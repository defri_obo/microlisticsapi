<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DB;
use Session;
use Exception;
use JWTAuth;
use Password;


class ResetPasswordController extends Controller
{
    public function __construct(){
        $this->middleware('cors');
        $this->middleware('loginAuth');
        $this->middleware('jwt.verify');  
    }
    
    public function forgot_password(Request $request){
        $input        = $request->all();
        $rules = array(
            'web_user' => "required",
            'email'    => "required|email",
        );
        $validator = Validator::make($input, $rules);
        $web_user = $request->web_user;
        $email    = $request->email;
        // dd($web_user, $email);
        $user_detail = DB::table('IW_WEB_USER')                                   
        ->select('EMAIL', 'NAME')
        ->where('WEB_USER', '=', $web_user)
        ->where('EMAIL', '=', $email)
        ->first();
        // dd($user_detail->name);
        $name     = $user_detail->name;
        $emailTo  = $user_detail->email;
        // $name         = 'name';
        $new_password = 'new_password';
        $data = [
            'name'         => $name,
            'new_password' => $new_password,
        ];
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                Mail::send('mailTemplates.testEmail', $data, function($message) use ($emailTo)
                {
                    $message->to($emailTo, 'Password')
                            ->from('yohanes.backup22@gmail.com', 'Reset Password')
                            ->subject('Reset Password');
                });                       
                $arr = array("status" => 200, "message" => "Email has been sent!");
            } catch (\Swift_TransportException $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage());
            } catch (Exception $ex) {
                $arr = array("status" => 400, "message" => $ex->getMessage());
            }
        }
        return \Response::json($arr);
    }
    
    // public function forgot_password(Request $request){
    //     $input = $request->all();
    //     $rules = array(
    //         'email' => "required|email",
    //     );
    //     $validator = Validator::make($input, $rules);
    //     if ($validator->fails()) {
    //         $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    //     } else {
    //         try {
    //             $response = Password::sendResetLink($request->only('email'), function (Message $message) {
    //                 $message->subject($this->getEmailSubject());
    //             });
    //             switch ($response) {                    
    //                 case Password::RESET_LINK_SENT:
    //                     return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
    //                 case Password::INVALID_USER:
    //                     return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
    //             }
    //         } catch (\Swift_TransportException $ex) {
    //             $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
    //         } catch (Exception $ex) {
    //             // dd($ex);
    //             $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
    //         }
    //     }
    //     return \Response::json($arr);
    // }

    public function change_password(Request $request){
        $input  = $request->all();
        $userid = Auth::guard('api')->user()->id;
        $rules  = array(
            'old_password'     => 'required',
            'new_password'     => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $arr = array("status" => 400, "message" => "Check your old password.", "data" => array());
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => 200, "message" => "Password updated successfully.", "data" => array());
                }
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => 400, "message" => $msg, "data" => array());
            }
        }
        return \Response::json($arr);
    }
}