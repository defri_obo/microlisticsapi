<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://127.0.0.1:8000/*',
        'http://3.19.207.202/*',
        'http://localhost/microlisticsapi/*',
        'http://api.microlistics.tech/*',
        'http://apidev.microlistics.tech/*',
        'https://api.microlistics.tech/*',
        'https://apidev.microlistics.tech/*',
        'http://localhost:8080/microlisticsapi/*',
        'http://localhost:8000/*'
    ];
}
