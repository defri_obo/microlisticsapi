<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
class LoginAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            $token     = $request->header('Authorization');
            $userLevel = $request->header('userLevel');

            if($token == null || $userLevel == null){
                return response()->json(['error message'=> "Bad request. invalid headers"],400);
            }

            return $next($request);
    }
}
